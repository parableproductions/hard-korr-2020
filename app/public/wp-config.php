<?php
define('WP_CACHE', true); // Added by WP Rocket
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'local' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', 'root' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '8fTfDyyaMdttl7ubZ+/meW6p+k5/y6+ZAD+2axggb4fViauVqQEc+6J1k306LANR5m+HcJ5gSGbD33Lqlu2EZQ==');
define('SECURE_AUTH_KEY',  'yHVmIVrs9EKByLvMPryjEFLIMp7AetJ/5j7tkpWaP4TAeM6rKWfAcasYz31aCH8vEhfGbF5Fh9DKsjULV5Vukg==');
define('LOGGED_IN_KEY',    'uEP6G7otoMbZk6FosOKTTJdfUyIZRk1z9MqzNZ4fK3CioqbEI9TNqV7VEU6OufiOnEcrxFRTVztZ1Sz/e1M6Rg==');
define('NONCE_KEY',        'BQGVke2ja9hmNfr0ufISuzGby6XCtXrmtHd1osNsl5x92EZm2WHh/Ah7NG7MTZAXBMSe8WPifoe9hUvAGcq6ow==');
define('AUTH_SALT',        'EcSdtkqsHQ/pSlY3PVyrsnyGugdgkDdzQIp2kE519+FhNdx2AmSGVdRXpFdRfPlIVPdLXiueqJV+4YKOxnNGCA==');
define('SECURE_AUTH_SALT', 'vGmmozF6lAkAZIkz/zDz+NQKLYpV8hyarCygC9zwTInAhVhIh14HGAb4daHGdJ20dFdMwd2wpJKVZa9wO/FoxQ==');
define('LOGGED_IN_SALT',   'Xev0j6FNiwRzUjBgQE1DMx0QQ/ZSvMv7er9+xZX7aN8u6S0tBfkfzZkhWTdC0Ofr9i4hBTZV2u5+sPnFFGQg8A==');
define('NONCE_SALT',       'jGS6FXZ1JGQW6C+hyEZIZICANRLerCqaouRvU6PiOnqnlZu/E5djt78Us3zg1WPbP14VaKWv0Fr1jI2dqncphA==');

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_vwobuxpx4r_';




/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';


