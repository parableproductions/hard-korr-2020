
<?php
/* Remove default footer and add new header
---------------------------------------------------------------------------- */
remove_action( 'genesis_after_header', 'genesis_do_nav' );
remove_action( 'genesis_header', 'genesis_do_header' );


function hardkorr_header(){ ?>

<!--<img style="width: 100%; height: fit-content;" src="<?php bloginfo('stylesheet_directory'); ?>/images/iron-grip.png" alt="<?php echo $image['alt']; ?>" />
<div>
    <span class="top-left text-white">25 Old Pacific Highway, YATALA QLD 4207 Australia</span>
    <span class="top-right text-white"><i class="fas fa-phone"></i> 1300 HRD KOR <i class="fas fa-envelope"></i> sales@hardkorrcampers.com.au <i class="fab fa-facebook-square"></i> <i class="fab fa-instagram"></i> <i
                class="fab fa-youtube"></i></span>
</div>

<img class="image-with-text" src="<?php bloginfo('stylesheet_directory'); ?>/images/checker-plate-background-small.jpg" alt="<?php echo $image['alt']; ?>" />
<img class="hkc-logo" src="<?php bloginfo('stylesheet_directory'); ?>/images/hard-korr-campers-logo.png" alt="<?php echo $image['alt']; ?>" />-->


<div class="header-wrapper">
    
    <div class="header-top" style="background-image: url('<?php bloginfo('stylesheet_directory'); ?>/images/iron-grip.png'); background-repeat: no-repeat; background-size: cover;">
        <div class="uk-container-custom">
            <div class="top-wrapper">
                <div class="left-panel">
                    <a href="https://goo.gl/maps/SRrd8iuwPFSkxQAa6" target="_blank">25 Old Pacific Highway, YATALA QLD 4207 Australia</a>
                </div>
                <div class="right-panel">
                    <div class="right-panel__phone">
                        <a href="tel:1300HRDKOR"><i class="fas fa-phone"></i> 1300 HRD KOR</a>
                    </div>
                    <div class="right-panel__email">
                        <a href="mailto:sales@hardkorrcampers.com.au "><i class="fas fa-envelope"></i>sales@hardkorrcampers.com.au</a>
                    </div>
                    <div class="right-panel__social">
                        <a target="_blank" href="https://www.facebook.com/hardkorrcampers"><i class="fab fa-facebook-square"></i> </a> 
                        <a target="_blank" href="https://www.instagram.com/hardkorrcampers"><i class="fab fa-instagram"></i> </a>
                        <a target="_blank" href="https://www.youtube.com/channel/UCrc_h4v78kliUdgADO0Yygg"><i class="fab fa-youtube"></i></a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="header-bottom" style="background-image: url('<?php bloginfo('stylesheet_directory'); ?>/images/checker-plate-background-small.jpg'); background-repeat: no-repeat; background-size: cover;">
    <div class="overlay-header"></div>
        <div class="uk-container-custom mobile-padd">
                <div class="bottom-wrapper"> 
                    <div class="left-panel"><a href="/"><img class="hkc-logo" src="<?php bloginfo('stylesheet_directory'); ?>/images/hard-korr-campers-logo.png" alt="<?php echo $image['alt']; ?>" /></a></div>
                    <div class="right-panel">

                        <nav id="menu" class="menu">
                            <div class="menu__inner">
                                <div class="hamburger">
                                    <span></span>
                                    <span></span>
                                    <span></span>
                                </div>
                                <ul>
                                    <li class="dropdown active"><a href="#">Campers</a>
                                        <div class="dropdown-content">
                                            <a href="http://hard-korr-campers-2020.local/product/overlander-gts/">Overlander GTS</a>
                                            <a href="http://hard-korr-campers-2020.local/product/overlander-gt/">Overlander GT</a>
                                            <a href="http://hard-korr-campers-2020.local/product/bushmaster-hkc-4000-platinum/">Bushmaster HKC-4000 Platinum</a>
                                            <a href="http://hard-korr-campers-2020.local/product/bushmaster-hkc-3600-platinum/">Bushmaster HKC-3600 Platinum</a>
                                        </div>
                                    </li>
                                    <li class="dropdown"><a href="#">Accesories</a>
                                    <div class="dropdown-content">
                                            <a href="#">LED Lighting & Solar</a>
                                            <a href="#">12V Fridge</a>
                                            <a href="#">Deep Cycle Betteries</a>
                                            <a href="#">Portable Hot Water</a>
                                            <a href="#">Other Accessories</a>
                                        </div>
                                    </li>
                                    <li><a href="http://hard-korr-campers-2020.local/dealer-location/">Dealers</a></li>
                                    <li><a href="http://hard-korr-campers-2020.local/events/">Shows</a></li>
                                    <li><a href="http://hard-korr-campers-2020.local/gallery/">Gallery</a></li>
                                    <li><a href="http://hard-korr-campers-2020.local/help-centre/">Help Centre</a></li>
                                    <li><a href="http://hard-korr-campers-2020.local/about/">About</a></li>
                                    <li><a href="http://hard-korr-campers-2020.local/contact-us/">Contact</a></li>
                                </ul>
                            </div>
                        </nav>

                        <!--<div class="menu-wrapper">
                            <nav class="menu">
                                <ul>
                                    <li class="sub-menu"><a href="#">Campers</a>
                                        <ul>
                                            <li><a href="#">Overlander GTS</a></li>
                                            <li><a href="#">Overlander GT</a></li>
                                            <li><a href="#">Bushmaster HKC-4000 Platinum</a></li>
                                            <li><a href="#">Bushmaster HKC-3600 Platinum</a></li>
                                        </ul>
                                    </li>
                                    <li class="sub-menu"><a href="#">Accesories</a> 
                                        <ul>
                                            <li><a href="#">LED Lighting $Solar</a></li>
                                            <li><a href="#">12V Fridges</a></li>
                                            <li><a href="#">Portable Cycle Batteries</a></li>
                                            <li><a href="#">Portable Hot Water</a></li>
                                            <li><a href="#">Other Accessories</a></li>
                                        </ul>
                                    </li>
                                    <li><a href="#">Dealers</a></li>
                                    <li><a href="#">Shows</a></li>
                                    <li><a href="#">Gallery</a></li>
                                    <li><a href="#">Help Centre</a></li>
                                    <li><a href="#">About</a></li>
                                    <li><a href="#">Contact</a></li>
                                </ul>
                            </nav>
                        </div>-->

                    </div>
                </div>
            </div>
        <!-- mobile menu-->

        <!--<nav class="mobile-menu">
            <div class="mobile-logo">
                <img class="hkc-logo" src="<?php bloginfo('stylesheet_directory'); ?>/images/hard-korr-campers-logo.png" alt="<?php echo $image['alt']; ?>" />
            </div>
            <div class="hamburger-container">
                <ul class="hamburger">
                    <li></li>
                    <li></li>
                    <li></li>
                </ul>
            </div>
            <ul>
                <li class="sub-menu"><a href="#">Campers</a>
                    <ul>
                        <li><a href="#">item</a></li>
                        <li><a href="#">item</a></li>
                    </ul>
                </li>
                <li><a href="#">Accesories</a></li>
                <li><a href="#">Dealers</a></li>
                <li><a href="#">Shows</a></li>
                <li><a href="#">Gallery</a></li>
                <li><a href="#">Help Centre</a></li>
                <li><a href="#">About</a></li>
                <li><a href="#">Contact</a></li>
            </ul>
        </nav> -->

        <!-- // mobile menu-->
    </div>
</div>

<div class="side-button-wrap">
    <a href="#contact-model" rel="modal:open" class="side-butto-wrap__btn uk-button uk-button-orange">ENQUIRE NOW</a>
</div>


<!-- Modal HTML embedded directly into document -->
<div id="contact-model" class="modal">
    <?= do_shortcode('[contact-form-7 id="653" title="Contact Us"]'); ?>
</div>



<?php
}

add_action( 'genesis_header', 'hardkorr_header' );