jQuery(document).ready(function($) {
    initClassMenuItem();
    //initClassMenuItemSize();
    initPopUpContact();
});

function initClassMenuItem() {
    $('.hamburger').click(function(){
            $(this).toggleClass('open');

        if($(this).hasClass('open') && !$('#menu')){
            $('#menu ul').slideDown();
        }else{
            $('#menu ul').slideToggle(400);
        }
    });
}

function initClassMenuItemSize() {
    $(window).scroll(function(){
        $(document.body).toggleClass('scrolled-down', ($(window).scrollTop() > 100));
    });
}

function initPopUpContact(){
    $("#contact-model").modal({
        fadeDuration: 100
      });
}
//this a temporary fix for the woocommerce

$('.woocommerce-tabs > .x-nav > .x-nav-tabs-item > a').on('click', function() {
	
	var index = $(this).data( 'cs-tab-toggle' );
	
	setTimeout( function() {
	$( '.woocommerce-tabs > .x-tab-content > div[data-cs-tab-index="' + index + '"] .x-nav > .x-nav-tabs-item:first-child a' ).trigger('click');
	}, 200 );
	
	} );
	
	$( document ).ready ( function() {
	
	$('.woocommerce-tabs > .x-nav > .x-nav-tabs-item:first-child > a').trigger('click');
	
	


/*  Please add this code to Admin > Appearance > Customizer > Custom > Javascript


    jQuery ( function($) {

        $('.woocommerce-tabs > .x-nav > .x-nav-tabs-item > a').on('click', function() {
        
        var index = $(this).data( 'cs-tab-toggle' );
        
        setTimeout( function() {
        $( '.woocommerce-tabs > .x-tab-content > div[data-cs-tab-index="' + index + '"] .x-nav > .x-nav-tabs-item:first-child a' ).trigger('click');
        }, 200 );
        
        } );
        
        $( document ).ready ( function() {
        
        $('.woocommerce-tabs > .x-nav > .x-nav-tabs-item:first-child > a').trigger('click');
        
        } );
        
        } );

        */