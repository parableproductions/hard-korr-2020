<?php
remove_action( 'genesis_footer', 'genesis_do_footer' );
function hardkorr_footer(){ ?>


<div class="footer-wrapper" style="background-image: url('<?php bloginfo('stylesheet_directory'); ?>/images/footer-bg.jpg'); background-repeat: no-repeat; background-size: cover;">
    <div class="overlay-footer"></div>
    <!--<img style="width: 100%;" src="<?php bloginfo('stylesheet_directory'); ?>/images/footer-bg.jpg" alt="<?php echo $image['alt']; ?>" />-->
    <div class="uk-container">
        <div class="footer-wrapper__wrap">
            <div class="footer-wrapper__wrap--left">
                <a href="<?php bloginfo('url'); ?>"><img src="http://hard-korr-campers-2020.local/wp-content/uploads/footer-logo.png" width="300" class="uk-margin-medium-bottom" title="<?php $bloginfo = get_bloginfo( $show, $filter ); echo $bloginfo; ?>" /></a>
                
                <p>Copyright © 2020 Hard Korr Campers Pty Ltd. <br>All Rights Reserved.</p>
                <p><a href="#" target="_blank">Sitemap</a>|<a href="#" target="_blank">Privacy Policy</a></p>
                <p>Website by MACK Marketing & New Adventure Media</p>

                <div class="social-icons"> 
                    <a href="#"><i class="fab fa-facebook-square fa-lg uk-margin-small-right"></i></a> 
                    <a href="#"><i class="fab fa-instagram fa-lg uk-margin-small-right"></i></a>
                    <a href="#"><i class="fab fa-youtube fa-lg uk-margin-small-right"></i></a>
                </div>
            </div>

            <div class="footer-wrapper__wrap--right">
                <h4>Search</h4>
                <div class="search-div">
                    <!--<input class="input-bar uk-margin-small-bottom" type="text" id="search" name="search">-->
                    
                    <?= do_shortcode('[ivory-search id="804" title="Default Search Form"]'); ?>
                    <!--<i class="fas fa-search"></i>-->
                </div>
                
                <div class="footer-menu">
                    <h4>Menu</h4>
                    <ul class="footer-menu__list">
                        <li class="footer-menu__list--item"><a target="_blank" href="#">Home</a></li>
                        <li class="footer-menu__list--item"><a target="_blank" href="#">Camper Trailers</a></li>
                        <li class="footer-menu__list--item"><a target="_blank" href="#">Find a Dealer</a></li>
                        <li class="footer-menu__list--item"><a target="_blank" href="#">Upcoming Shows</a></li>
                        <li class="footer-menu__list--item"><a target="_blank" href="#">Help Center</a></li>
                        <li class="footer-menu__list--item"><a target="_blank" href="#">About Us</a></li>
                        <li class="footer-menu__list--item"><a target="_blank" href="#">Contact Us</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <div class="comment-wrap">
        <i class="fas fa-comment"></i>
    </div>
</div>



<script>
$('.slider-syncing-script').slick({
  infinite: true,
  slidesToShow: 3,
  slidesToScroll: 3
});
</script>

<?php
    }
    
    add_action( 'genesis_footer', 'hardkorr_footer' );