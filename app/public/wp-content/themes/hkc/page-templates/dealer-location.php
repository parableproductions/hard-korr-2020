<?php // Template Name: Dealer Location ?>
<?php get_header();?>

<!-- Header -->
<section class="header-section">   
    <div class="header-overlay">
        <?php $thumbnail = get_the_post_thumbnail(); ?>
        <img style="width: 100%;" src="<?php echo $thumbnail;?>">
    </div>
    <h1 class="page-title"><?php echo the_title();?></h1>
    <div class="bottom-chevron">
            <img src="http://hard-korr-campers-2020.local/wp-content/uploads/chevron-bottom.svg" width="100%">
        </div>
</section> 
<!-- /header -->

<div class="uk-container">
    <?php echo the_content();?>
    <div class="store-section">

        <!-- $args = array(
'post_type' => 'wpsl_store',
"taxonomy" => "wpsl_store_category"  );
$categories = get_categories($args);
foreach ($categories as $category) :
<echo $category->name; 
wp_reset_query();-->

        <!-- Queensland -->
        <h3>Queensland</h3>
        <hr>
        <div class="dealer-stores">
            <div class="uk-child-width-1-3@m uk-grid-small uk-grid-match" uk-grid>

                <div>
                    <div class="uk-card uk-card-default uk-card-body">
                        <a target="_blank" href="<?php echo home_url(); ?>">
                            <img class="store-logo" src="http://hard-korr-campers-2020.local/wp-content/uploads/favicon-new.png">
                            <div class="store-title">
                                Hard Korr Campers
                            </div>
                        </a>
                        <div class="store-address">
                            25 Old Pacific Hwy <br>
                            Yatala Queensland 4207 <br>
                            <br>
                            P: (07) 3801 8332
                        </div>
                        <div uk-toggle="target: #modal-close-default" class="btn-contact uk-text-center">
                            Contact this dealer <i class="fas fa-angle-double-right fa-xs"></i>
                        </div>
                    </div>
                </div>

                <div id="modal-close-default" uk-modal>
                    <div class="uk-modal-dialog uk-modal-body">
                        <button class="uk-modal-close-default" type="button" uk-close></button>
                        <?php echo do_shortcode('[contact-form-7 id="735" title="Contact Hard Korr Campers"]'); ?>
                    </div>
                </div>

                <div>
                    <div class="uk-card uk-card-default uk-card-body">
                        <a target="_blank" href="https://www.ozziecampers.com.au/">
                            <img class="store-logo"
                                src="http://hard-korr-campers-2020.local/wp-content/uploads/ozzie-campers-logo-150x150-1.png">
                            <div class="store-title">
                                Ozzie Camper Trailers
                            </div>
                        </a>
                        <div class="store-address">
                            48 Charles St <br>
                            Aitkenvale QLD 4814 <br>
                            <br>
                            P: (07) 4779 0900
                        </div>
                        <div uk-toggle="target: #modal-close-default" class="btn-contact uk-text-center">
                            Contact this dealer <i class="fas fa-angle-double-right fa-xs"></i>
                        </div>
                    </div>
                </div>

                <div id="modal-close-default" uk-modal>
                    <div class="uk-modal-dialog uk-modal-body">
                        <button class="uk-modal-close-default" type="button" uk-close></button>
                        <?php echo do_shortcode('[contact-form-7 id="736" title="Contact Ozzie Camper Trailers"]'); ?>
                    </div>
                </div>

                <div>
                    <div class="uk-card uk-card-default uk-card-body">
                        <a target="_blank" href="https://www.campersqld.com.au/">
                            <img class="store-logo"
                                src="http://hard-korr-campers-2020.local/wp-content/uploads/campers-queensland-logo-100x100-1.png">
                            <div class="store-title">
                                Campers Queensland
                            </div>
                        </a>
                        <div class="store-address">
                            Unit 8, 13-17 Carl Court <br>
                            Rural View QLD 4740 <br>
                            <br>
                            P: 0456 226 737
                        </div>
                        <div uk-toggle="target: #modal-close-default" class="btn-contact uk-text-center">Contact this
                            dealer
                            <i class="fas fa-angle-double-right fa-xs"></i></div>
                    </div>

                    <div id="modal-close-default" uk-modal>
                        <div class="uk-modal-dialog uk-modal-body">
                            <button class="uk-modal-close-default" type="button" uk-close></button>
                            <?php echo do_shortcode('[contact-form-7 id="737" title="Contact Campers Queensland"]'); ?>
                        </div>
                    </div>
                </div>

            </div>
        </div>
        <!-- /Queensland -->

        <!-- New South Wales / ACT -->
        <h3>New South Wales / ACT</h3>
        <hr>
        <div class="dealer-stores">
            <div class="uk-child-width-1-3@m uk-grid-small uk-grid-match" uk-grid>

                <div>
                    <div class="uk-card uk-card-default uk-card-body">
                        <a target="_blank" href="https://www.carnet.com.au/">
                            <img class="store-logo"
                                src="http://hard-korr-campers-2020.local/wp-content/uploads/carnet-logo-150x50-1.png">
                            <div class="store-title">
                                CarNet Western Sydney
                            </div>
                        </a>
                        <div class="store-address">
                            Cnr Winford Drive & Grier Crossing <br>
                            McGraths Hill NSW 2756 <br>
                            <br>
                            P: 0488 333 073
                        </div>
                        <div uk-toggle="target: #modal-close-default" class="btn-contact uk-text-center">Contact this
                            dealer
                            <i class="fas fa-angle-double-right fa-xs"></i></div>
                    </div>
                </div>

                <div id="modal-close-default" uk-modal>
                    <div class="uk-modal-dialog uk-modal-body">
                        <button class="uk-modal-close-default" type="button" uk-close></button>
                        <?php echo do_shortcode('[contact-form-7 id="738" title="Contact CarNet Western Sydney"]'); ?>
                    </div>
                </div>

                <div>
                    <div class="uk-card uk-card-default uk-card-body">
                        <a target="_blank" href="https://www.wowcampersandtrailers.com.au/">
                            <img class="store-logo"
                                src="http://hard-korr-campers-2020.local/wp-content/uploads/wow-campers-trailers-logo-100x100-1.png">
                            <div class="store-title">
                                WOW Campers & Trailers Superstore
                            </div>
                        </a>
                        <div class="store-address">
                            Cnr Molesworth St & Ballina Rd <br>
                            Lismore NSW 2480 <br>
                            <br>
                            P: (02) 6621 5320
                        </div>
                        <div uk-toggle="target: #modal-close-default" class="btn-contact uk-text-center">Contact this
                            dealer
                            <i class="fas fa-angle-double-right fa-xs"></i></div>
                    </div>
                </div>

                <div id="modal-close-default" uk-modal>
                    <div class="uk-modal-dialog uk-modal-body">
                        <button class="uk-modal-close-default" type="button" uk-close></button>
                        <?php echo do_shortcode('[contact-form-7 id="739" title="Contact WOW Campers & Trailers Superstore"]'); ?>
                    </div>
                </div>

                <div>
                    <div class="uk-card uk-card-default uk-card-body">
                        <a target="_blank" href="https://coffscanvas.com.au/">
                            <img class="store-logo"
                                src="http://hard-korr-campers-2020.local/wp-content/uploads/coffs-canvas-logo-150x150-1.png">
                            <div class="store-title">
                                Coffs Canvas & Campers
                            </div>
                        </a>
                        <div class="store-address">
                            1 Keona Circuit <br>
                            North Boambee Valley NSW 2450 <br>
                            <br>
                            P: (02) 6651 2960
                        </div>
                        <div uk-toggle="target: #modal-close-default" class="btn-contact uk-text-center">Contact this
                            dealer <i class="fas fa-angle-double-right fa-xs"></i></div>
                    </div>
                </div>

                <div id="modal-close-default" uk-modal>
                    <div class="uk-modal-dialog uk-modal-body">
                        <button class="uk-modal-close-default" type="button" uk-close></button>
                        <?php echo do_shortcode('[contact-form-7 id="740" title="Contact Coffs Canvas & Campers"]'); ?>
                    </div>
                </div>

            </div>
        </div>
        <!-- /News South Wales / ACT-->

        <!-- Victoria-->
        <h3>Victoria</h3>
        <hr>
        <div class="dealer-stores">
            <div class="uk-child-width-1-3@m uk-grid-small uk-grid-match" uk-grid>

                <div>
                    <div class="uk-card uk-card-default uk-card-body">
                        <a target="_blank"
                            href="https://www.tackleworld.com.au/store/alpine-country-tackle-world-sale/">
                            <img class="store-logo"
                                src="http://hard-korr-campers-2020.local/wp-content/uploads/tackle-world-logo.png">
                            <div class="store-title">
                                Alpine Country Tackle World Sale
                            </div>
                        </a>
                        <div class="store-address">
                            82 MacArthur St <br>
                            Sale VIC 3850 <br>
                            <br>
                            P: (03) 5144 7505
                        </div>
                        <div uk-toggle="target: #modal-close-default" class="btn-contact uk-text-center">Contact this
                            dealer
                            <i class="fas fa-angle-double-right fa-xs"></i></div>
                    </div>
                </div>

                <div id="modal-close-default" uk-modal>
                    <div class="uk-modal-dialog uk-modal-body">
                        <button class="uk-modal-close-default" type="button" uk-close></button>
                        <?php echo do_shortcode('[contact-form-7 id="741" title="Contact Alpine Country Tackle World Sale"]'); ?>
                    </div>
                </div>

                <div>
                    <div class="uk-card uk-card-default uk-card-body card-hidden">
                        <img class="store-logo" src="http://hard-korr-campers-2020.local/wp-content/uploads/favicon-new.png">
                        <div class="store-title">
                        </div>
                        <div class="store-address">
                            <br>
                            <br>
                            <br>
                        </div>
                        <div class="btn-contact uk-text-center">
                        </div>
                    </div>
                </div>

                <div>
                    <div class="uk-card uk-card-default uk-card-body card-hidden">
                        <img class="store-logo" src="http://hard-korr-campers-2020.local/wp-content/uploads/favicon-new.png">
                        <div class="store-title">
                        </div>
                        <div class="store-address">
                            <br>
                            <br>
                            <br>
                        </div>
                        <div class="btn-contact uk-text-center">
                        </div>
                    </div>
                </div>

            </div>
        </div>
        <!-- /Victoria -->

        <!-- Tasmania -->
        <h3>Tasmania</h3>
        <hr>
        <div class="dealer-stores">
            <div class="uk-child-width-1-3@m uk-grid-small uk-grid-match" uk-grid>

                <div>
                    <div class="uk-card uk-card-default uk-card-body">
                        <a target="_blank" href="http://camperstas.com.au/index.html">
                            <img class="store-logo"
                                src="http://hard-korr-campers-2020.local/wp-content/uploads/campers-tasmania-logo.png">
                            <div class="store-title">
                                Campers Tasmania
                            </div>
                        </a>
                        <div class="store-address">
                            85 Frankland St <br>
                            Launceston TAS 7250 <br>
                            <br>
                            P: 0458 226 737
                        </div>
                        <div uk-toggle="target: #modal-close-default" class="btn-contact uk-text-center">Contact this
                            dealer
                            <i class="fas fa-angle-double-right fa-xs"></i></div>
                    </div>
                </div>

                <div id="modal-close-default" uk-modal>
                    <div class="uk-modal-dialog uk-modal-body">
                        <button class="uk-modal-close-default" type="button" uk-close></button>
                        <?php echo do_shortcode('[contact-form-7 id="742" title="Contact Campers Tasmania"]'); ?>
                    </div>
                </div>

                <div>
                    <div class="uk-card uk-card-default uk-card-body card-hidden">
                        <img class="store-logo" src="http://hard-korr-campers-2020.local/wp-content/uploads/favicon-new.png">
                        <div class="store-title">
                        </div>
                        <div class="store-address">
                            <br>
                            <br>
                            <br>
                        </div>
                        <div class="btn-contact uk-text-center">
                        </div>
                    </div>
                </div>

                <div>
                    <div class="uk-card uk-card-default uk-card-body card-hidden">
                        <img class="store-logo" src="http://hard-korr-campers-2020.local/wp-content/uploads/favicon-new.png">
                        <div class="store-title">
                        </div>
                        <div class="store-address">
                            <br>
                            <br>
                            <br>
                        </div>
                        <div class="btn-contact uk-text-center">
                        </div>
                    </div>
                </div>

            </div>
        </div>
        <!-- /Tasmania -->

        <!-- South Australia -->
        <h3>South Australia</h3>
        <hr>
        <div class="dealer-stores">
            <div class="uk-child-width-1-3@m uk-grid-small uk-grid-match" uk-grid>

                <div>
                    <div class="uk-card uk-card-default uk-card-body">
                        <a target="_blank" href="https://mountbarker4x4centre.com.au/">
                            <img class="store-logo"
                                src="http://hard-korr-campers-2020.local/wp-content/uploads/mb4x4c-logo-150x150-1.jpg">
                            <div class="store-title">
                                Mt Barker 4×4 Centre
                            </div>
                        </a>
                        <div class="store-address">
                            1 Mount Barker Road <br>
                            Mount Barker SA 5250 <br>
                            <br>
                            P: (08) 8391 4391
                        </div>
                        <div uk-toggle="target: #modal-close-default" class="btn-contact uk-text-center">Contact this
                            dealer
                            <i class="fas fa-angle-double-right fa-xs"></i></div>
                    </div>
                </div>

                <div id="modal-close-default" uk-modal>
                    <div class="uk-modal-dialog uk-modal-body">
                        <button class="uk-modal-close-default" type="button" uk-close></button>
                        <?php echo do_shortcode('[contact-form-7 id="743" title="Contact Mt Barker 4×4 Centre"]'); ?>
                    </div>
                </div>

                <div>
                    <div class="uk-card uk-card-default uk-card-body card-hidden">
                        <img class="store-logo" src="http://hard-korr-campers-2020.local/wp-content/uploads/favicon-new.png">
                        <div class="store-title">
                        </div>
                        <div class="store-address">
                            <br>
                            <br>
                            <br>
                        </div>
                        <div class="btn-contact uk-text-center">
                        </div>
                    </div>
                </div>

                <div>
                    <div class="uk-card uk-card-default uk-card-body card-hidden">
                        <img class="store-logo" src="http://hard-korr-campers-2020.local/wp-content/uploads/favicon-new.png">
                        <div class="store-title">
                        </div>
                        <div class="store-address">
                            <br>
                            <br>
                            <br>
                        </div>
                        <div class="btn-contact uk-text-center">
                        </div>
                    </div>
                </div>

            </div>
        </div>
        <!-- /South Australia -->

        <!-- Western Australia-->
        <h3>Western Australia</h3>
        <hr>
        <div class="dealer-stores">
            <div class="uk-child-width-1-3@m uk-grid-small uk-grid-match" uk-grid>

                <div>
                    <div class="uk-card uk-card-default uk-card-body">
                        <a target="_blank" href="https://www.westozcampertrailers.com.au/">
                            <img class="store-logo"
                                src="http://hard-korr-campers-2020.local/wp-content/uploads/west-oz-camper-trailers-logo-100x100-1.png">
                            <div class="store-title">
                                West Oz Camper Trailers
                            </div>
                        </a>
                        <div class="store-address">
                            2/41 Mulgul Road <br>
                            Malaga WA 6069 <br>
                            <br>
                            P: (08) 9249 9397
                        </div>
                        <div uk-toggle="target: #modal-close-default" class="btn-contact uk-text-center">Contact
                            this
                            dealer <i class="fas fa-angle-double-right fa-xs"></i></div>
                    </div>
                </div>

                <div id="modal-close-default" uk-modal>
                    <div class="uk-modal-dialog uk-modal-body">
                        <button class="uk-modal-close-default" type="button" uk-close></button>
                        <?php echo do_shortcode('[contact-form-7 id="744" title="Contact West Oz Camper Trailers"]'); ?>
                    </div>
                </div>

                <div>
                    <div class="uk-card uk-card-default uk-card-body card-hidden">
                        <img class="store-logo" src="http://hard-korr-campers-2020.local/wp-content/uploads/favicon-new.png">
                        <div class="store-title">
                        </div>
                        <div class="store-address">
                            <br>
                            <br>
                            <br>
                        </div>
                        <div class="btn-contact uk-text-center">
                        </div>
                    </div>
                </div>

                <div>
                    <div class="uk-card uk-card-default uk-card-body card-hidden">
                        <img class="store-logo" src="http://hard-korr-campers-2020.local/wp-content/uploads/favicon-new.png">
                        <div class="store-title">
                        </div>
                        <div class="store-address">
                            <br>
                            <br>
                            <br>
                        </div>
                        <div class="btn-contact uk-text-center">
                        </div>
                    </div>
                </div>

            </div>
        </div>
        <!-- /Western Australia -->
    </div>

    <!-- Enquire -->
    
<div class="mt-100">
    <?php get_template_part( 'template-parts/template', 'enquiryform' ); ?>
</div>
<?php get_footer();?>