<?php // Template Name: Contact Us?>
<?php get_header();?>

<!-- Header -->
<section class="header-section">   
    <div class="header-overlay">
        <?php $thumbnail = get_the_post_thumbnail(); ?>
        <img style="width: 100%;" src="<?php echo $thumbnail;?>">
    </div>
    <h1 class="page-title"><?php echo the_title();?></h1>
    <div class="bottom-chevron">
            <img src="http://hard-korr-campers-2020.local/wp-content/uploads/chevron-bottom.svg" width="100%">
        </div>
</section> 
<!-- /header -->

<div class="uk-container">
    <?php echo the_content();?>
</div>

<?php get_footer();?>