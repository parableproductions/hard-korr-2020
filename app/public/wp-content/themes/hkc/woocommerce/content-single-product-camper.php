<?php
/**
 * The template for displaying CAMPER product content in the single-product.php template
 */
defined( 'ABSPATH' ) || exit;
global $product;
/**
 * Hook: woocommerce_before_single_product.
 *
 * @hooked wc_print_notices - 10
 */
do_action( 'woocommerce_before_single_product' );
if ( post_password_required() ) {
	echo get_the_password_form(); // WPCS: XSS ok.
	return;
}
$top_bg_image = get_field ('top_background_image');
$featureImg = wp_get_attachment_image_src( get_post_thumbnail_id( $loop->post->ID ), 'single-post-thumbnail' );
?>
<div class="camper-products">
<!-- Product Header -->
<div class="camper-header-overlay">
    <img class="top-bg-img" src="<?php echo esc_url($top_bg_image['url']); ?>"
        alt="<?php echo esc_attr($top_bg_image['alt']); ?>">
</div>
<div class="triangle-divider">
    <div class="triangles">
        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 100 100" preserveAspectRatio="none">
            <polygon fill="white" points="100,100 0,100, 0,0" />
        </svg>
        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 100 100" preserveAspectRatio="none">
            <polygon fill="white" points="0,100 100,0 100,100"></polygon>
        </svg>
    </div>
    <img data-src="/-/media/project/images/g2m/img-sem-hero-min-b-png.png?h=603&amp;w=1044&amp;la=en-AU&amp;hash=05BAE24430B4232FF0E197BB34F7BBA9"
        alt="" />
</div>
<div class="uk-flex uk-flex-center">
    <img class="product-feature-image" src="<?php  echo $featureImg[0]; ?>" data-id="<?php echo $loop->post->ID; ?>">
</div>
<!-- /Product header -->
<div class="uk-container">

    
 <!-- WOOCOMMERCE FULL WAPPER -->

<div class="top-woocommerce-wrapper">



        <!-- WOOCOMMERCE -->
    <div class="mt-100">
        <div id="product-<?php the_ID(); ?>" <?php wc_product_class( '', $product ); ?>>

        <div class="title-price-wrap">
               <!-- TITLE & PRICE -->
                <div class="title-wrapper">
                 
                        <?php
                            /**
                             * Hook: woocommerce_before_single_product_summary.
                             * @hooked woocommerce_show_product_sale_flash - 10
                             * @hooked woocommerce_show_product_images - 20
                             */
                            add_action( 'woocommerce_before_single_product_summary', 'woocommerce_template_single_title', 1 );

                            // add_action( 'woocommerce_before_single_product_summary', 'woocommerce_template_single_price', 2 );
                            // add_action( 'woocommerce_product_thumbnails', 'woocommerce_template_single_price', 2 );
                            remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_add_to_cart', 30 );
                            remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_meta', 40 );
                            remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_rating', 10 );
                            remove_action( 'woocommerce_before_single_product_summary', 'woocommerce_show_product_images', 20 );
                            do_action( 'woocommerce_before_single_product_summary' );
                        ?>
                </div>
                 <!-- /TITLE & PRICE -->

                <!-- Price onew -->
              <div class="price-wrapper">
                    <div id="product-<?php the_ID(); ?>" <?php wc_product_class( '', $product ); ?>>
                    <?php
                        add_action( 'woocommerce_price_summary', 'woocommerce_template_single_price', 1 );
                        do_action( 'woocommerce_price_summary' );
                        ?>
                    </div>
            </div>
        <!--/Price -->



              <!-- Price old 
              <div class="price-wrapper">
                <div class="uk-grid">
                    <div class="uk-width-1-2">
                        <div id="product-<?php the_ID(); ?>" <?php wc_product_class( '', $product ); ?>>

                            <?php
                            add_action( 'woocommerce_price_summary', 'woocommerce_template_single_price', 1 );
                            do_action( 'woocommerce_price_summary' );
                            ?>
                        </div>
                    </div>
                </div>
            </div>
             /Price -->
        </div>

           

            <!-- DESCRIPTION-->
            <div class="summary entry-summary">
                <?php
                /**
                 * Hook: woocommerce_single_product_summary.
                 * @hooked woocommerce_template_single_title - 5
                 * @hooked woocommerce_template_single_rating - 10
                 * @hooked woocommerce_template_single_price - 10
                 * @hooked woocommerce_template_single_excerpt - 20
                 * @hooked woocommerce_template_single_add_to_cart - 30
                 * @hooked woocommerce_template_single_meta - 40
                 * @hooked woocommerce_template_single_sharing - 50
                 * @hooked WC_Structured_Data::generate_product_data() - 60
                 */
                remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_title', 5 );
                remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_price', 10 );

                do_action( 'woocommerce_single_product_summary' );
                ?>
               
            </div>

           


        </div>
    </div>
    <!-- /DESCRIPTION -->

   

</div>

 <!-- WOOCOMMERCE FULL WAPPER -->


 <!-- FEATURE ICONS -->

<div class="icon-wrapper">
    <div class="uk-grid mt-100">
        <?php if( have_rows('feature_icons') ):
            while( have_rows('feature_icons') ): the_row();
            $feature_title = get_sub_field('feature_title');
            $feature_icon = get_sub_field('feature_icon');
            $feature_text = get_sub_field('feature_text');
            ?>
        <div class="uk-width-1-6 uk-text-center mobile-center">
            <img class="info_icon" src="<?php echo esc_url($feature_icon['url']); ?>"
                alt="<?php echo esc_attr($feature_icon['alt']); ?>">
            <h5 class="info-title"><?php echo $feature_title; ?></h5>
            <p class="info-text"><?php echo $feature_text; ?></p>
        </div>
        <? endwhile; ?>
        <?php endif; ?>
    </div>
</div>
    <!-- /FEATURE ICONS -->

    <!-- GALLERY -->
    <div class="mt-100 ">
        <div id="product-<?php the_ID(); ?>" <?php wc_product_class( '', $product ); ?>>

            <?php
    add_action( 'woocommerce_gallery_summary', 'woocommerce_show_product_images', 1 );
	do_action( 'woocommerce_gallery_summary' );
	?>
        </div>
    </div>
    <!-- /GALLERY -->

</div>




<!-- FEATURE BOX -->
<div class="feature-wrapper">
    <?php
// Check value exists.
if( have_rows('feature_box') ):
    // Loop through rows.
    while ( have_rows('feature_box') ) : the_row();
        // Layout LEFT
        if( get_row_layout() == 'layout_left' ):
			$content_left = get_sub_field('content_left');
            $image_left = get_sub_field('image_left'); ?>

    <div class="box-wrap" style="background-image: url('<?php echo esc_url($image_left['url']); ?>'); background-repeat: no-repeat; background-size: cover;">
        <div class="box-wrap__left"t>
            <div class="content-left"><?php echo $content_left;?></div>
        </div>
        <div class="feature-box-arrow-right"></div>
    </div>

    <?php
// Layout RIGHT
        elseif( get_row_layout() == 'layout_right' ): 
			$content_right = get_sub_field('content_right');
            $image_right = get_sub_field('image_right'); ?>


    <div class="box-wrap" style="background-image: url('<?php echo esc_url($image_right['url']); ?>'); background-repeat: no-repeat; background-size: cover;">
        <div class="box-wrap__right">
            <div class="content-right"><?php echo $content_right;?></div>
        </div>
        <div class="feature-box-arrow-left"></div>
    </div>



    
    <?php
        endif;
    endwhile;
else :
endif;?>
</div>
<!-- /FEATURE BOX -->




<div class="uk-container">
    <!--A TBS -->
    <div class="mt-100">
        <div id="product-<?php the_ID(); ?>" <?php wc_product_class( '', $product ); ?>>

            <?php
	/**
	 * Hook: woocommerce_after_single_product_summary.
	 *
	 * @hooked woocommerce_output_product_data_tabs - 10
	 * @hooked woocommerce_upsell_display - 15
	 * @hooked woocommerce_output_related_products - 20
	 */
    remove_action( 'woocommerce_after_single_product_summary', 'woocommerce_output_related_products', 20 );
	do_action( 'woocommerce_after_single_product_summary' );
	?>
        </div>
    </div>
    <!-- TABS -->

    


    <!--/Features Standard/Options -->

    <?php do_action( 'woocommerce_after_single_product' ); ?>


    <!--Features Standard/Options -->
    <div class="mt-100 uk-text-center">
        <?php if( have_rows('repeater_standard_option') ):
    while( have_rows('repeater_standard_option') ): the_row(); 
    $standard_img = get_sub_field ('features_standard_image');
    ?>
        <div class="uk-inline">
            <div> <img class="standard-img shadow" src="<?php echo ($standard_img['url']); ?>"
                    alt="<?php echo esc_attr($standard_img['alt']); ?>">


                <div class="uk-position-top uk-overlay uk-overlay-default">
                    <h5 class="standard-title"><?php echo the_sub_field('features_standard_title');?></h5>
                    <p class="standard-subtitle"><?php echo the_sub_field('features_standard_subtitle');?></p>
                </div>
            </div>
            <?php if( get_sub_field('features_standard_option') == 'Standard' ): ?>
            <div class="uk-overlay-default uk-position-bottom standard uk-text-uppercase">
                <p class=""> Standard</p>
            </div>
            <?php elseif( get_sub_field('features_standard_option') == 'Option' ): ?>
            <div class="uk-overlay-default uk-position-bottom option uk-text-uppercase">
                <p class=""> Option </p>
            </div>
            <?php endif; ?>
        </div>
        <? endwhile; ?>
        <?php endif; ?>
    </div>
</div>
<!-- /WOOCOMMERCE -->

<div class="mt-100">
    <?php get_template_part( 'template-parts/template', 'finddealer' ); ?>
</div>
<div class="mt-100">
    <?php get_template_part( 'template-parts/template', 'enquiryform' ); ?>
</div>

<!--<?php get_template_part( 'template-parts/enquiry' ); ?>-->

</div>

<script>
jQuery ( function($) {

$('.woocommerce-tabs > .x-nav > .x-nav-tabs-item > a').on('click', function() {

var index = $(this).data( 'cs-tab-toggle' );

setTimeout( function() {
$( '.woocommerce-tabs > .x-tab-content > div[data-cs-tab-index="' + index + '"] .x-nav > .x-nav-tabs-item:first-child a' ).trigger('click');
}, 200 );

} );

$( document ).ready ( function() {

$('.woocommerce-tabs > .x-nav > .x-nav-tabs-item:first-child > a').trigger('click');

} );

} );
</script>