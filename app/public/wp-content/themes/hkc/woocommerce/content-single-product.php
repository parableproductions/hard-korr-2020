<?php
/**
 * The template for displaying SINGLE product content in the single-product.php template
 */
defined( 'ABSPATH' ) || exit;
global $product;
/**
 * Hook: woocommerce_before_single_product.
 *
 * @hooked wc_print_notices - 10
 */
do_action( 'woocommerce_before_single_product' );
if ( post_password_required() ) {
	echo get_the_password_form(); // WPCS: XSS ok.
	return;
}
$top_bg_image = get_field ('top_background_image');
$featureImg = wp_get_attachment_image_src( get_post_thumbnail_id( $loop->post->ID ), 'single-post-thumbnail' );
?>

<!-- Header -->
<div class="header-triangle">
    <div class="header-overlay">
		<img style="width: 100%;" src="<?php echo esc_url($top_bg_image['url']); ?>"
        alt="<?php echo esc_attr($top_bg_image['alt']); ?>">
    </div>
    <div class="triangle-divider">
        <div class="triangles">
            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 100 100" preserveAspectRatio="none">
                <polygon fill="white" points="100,100 0,100, 0,0" />
            </svg>
            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 100 100" preserveAspectRatio="none">
                <polygon fill="white" points="0,100 100,0 100,100"></polygon>
            </svg>
        </div>
        <img
            data-src="/-/media/project/images/g2m/img-sem-hero-min-b-png.png?h=603&amp;w=1044&amp;la=en-AU&amp;hash=05BAE24430B4232FF0E197BB34F7BBA9" />
    </div>
</div>

<div class="uk-flex uk-flex-center">
    <img class="product-feature-image" src="<?php  echo $featureImg[0]; ?>" data-id="<?php echo $loop->post->ID; ?>">
</div>
<!-- /header -->












<div class="uk-container">

    <!-- Overview & Gallery -->
    <div class="mt-100">
        <div id="product-<?php the_ID(); ?>" <?php wc_product_class( '', $product ); ?>>

            <?php
	/**
	 * Hook: woocommerce_before_single_product_summary.
	 *
	 * @hooked woocommerce_show_product_sale_flash - 10
	 * @hooked woocommerce_show_product_images - 20
	 */
	do_action( 'woocommerce_before_single_product_summary' );
	?>

            <div class="summary entry-summary">
                <?php
		/**
		 * Hook: woocommerce_single_product_summary.
		 *
		 * @hooked woocommerce_template_single_title - 5
		 * @hooked woocommerce_template_single_rating - 10
		 * @hooked woocommerce_template_single_price - 10
		 * @hooked woocommerce_template_single_excerpt - 20
		 * @hooked woocommerce_template_single_add_to_cart - 30
		 * @hooked woocommerce_template_single_meta - 40
		 * @hooked woocommerce_template_single_sharing - 50
		 * @hooked WC_Structured_Data::generate_product_data() - 60
		 */
		do_action( 'woocommerce_single_product_summary' );
		?>
            </div>

            <?php
	/**
	 * Hook: woocommerce_after_single_product_summary.
	 *
	 * @hooked woocommerce_output_product_data_tabs - 10
	 * @hooked woocommerce_upsell_display - 15
	 * @hooked woocommerce_output_related_products - 20
	 */
	do_action( 'woocommerce_after_single_product_summary' );
	?>
        </div>

        <?php do_action( 'woocommerce_after_single_product' ); ?>

    </div>