<?php get_header(); ?>




    <?php
		/**
		 * woocommerce_before_main_content hook.
		 *
		 * @hooked woocommerce_output_content_wrapper - 10 (outputs opening divs for the content)
		 * @hooked woocommerce_breadcrumb - 20
		 */
		do_action( 'woocommerce_before_main_content' );
	?>

    <?php while ( have_posts() ) : the_post(); ?>

    <?php 
        global $post;
        $terms = wp_get_post_terms( $post->ID, 'product_cat' );
        foreach ( $terms as $term ) $categories[] = $term->slug;
        if ( in_array( 'camper', $categories ) ) {
        wc_get_template_part( 'content', 'single-product-camper' );
    } else {
        wc_get_template_part( 'content', 'single-product' );
    }
            
            // wc_get_template_part( 'content', 'single-product' ); ?>

    <?php endwhile; // end of the loop. ?>

    <?php
		/**
		 * woocommerce_after_main_content hook.
		 *
		 * @hooked woocommerce_output_content_wrapper_end - 10 (outputs closing divs for the content)
		 */
		do_action( 'woocommerce_after_main_content' );
	?>



<?php get_footer(); ?>