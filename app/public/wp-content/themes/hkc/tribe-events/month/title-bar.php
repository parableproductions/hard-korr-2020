<?php
/**
 * Month View Title Template
 * The title template for the month view of events.
 *
 * Override this template in your own theme by creating a file at [your-theme]/tribe-events/month/title-bar.php
 *
 * @package TribeEventsCalendar
 * @version 4.6.19
 * @since   4.6.19
 *
 */
?>

<!-- Header -->
<section class="header-section">
    <div class="header-overlay">
        <img style="width: 100%;"
            src="http://hard-korr-campers-2020.local/wp-content/uploads/istock_000008791096large1.png">
    </div>
    <h1 class="page-title">Shows</h1>
    <div class="bottom-chevron">
        <img src="http://hard-korr-campers-2020.local/wp-content/uploads/chevron-bottom.svg" width="100%">
    </div>
</section>
<!-- /header -->

<div class="uk-container">