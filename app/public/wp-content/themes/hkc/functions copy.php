<?php
/**
 * Hard Korr Campers.
 *
 * This file adds functions to the Hard Korr Campers Theme.
 *
 * @package Hard Korr Campers
 * @author  Mack Marketing
 * @license GPL-2.0-or-later
 * @link    https://mackmarketing.com.au
 */

/* Starts the engine
-------------------------------------------------------------------------------- */

require_once get_template_directory() . '/lib/init.php';

/* Sets up the theme
-------------------------------------------------------------------------------- */

require_once get_stylesheet_directory() . '/lib/theme-defaults.php';

/* Sets localisation (do not remove)
-------------------------------------------------------------------------------- */

add_action( 'after_setup_theme', 'genesis_sample_localization_setup' );

function genesis_sample_localization_setup() {

	load_child_theme_textdomain( genesis_get_theme_handle(), get_stylesheet_directory() . '/languages' );

}

/* Adds helper functions
-------------------------------------------------------------------------------- */

require_once get_stylesheet_directory() . '/lib/helper-functions.php';

/* Adds image upload and colour select to customiser.
-------------------------------------------------------------------------------- */

require_once get_stylesheet_directory() . '/lib/customize.php';

/* Includes customiser CSS.
-------------------------------------------------------------------------------- */

require_once get_stylesheet_directory() . '/lib/output.php';

/* Adds WooCommerce support.
-------------------------------------------------------------------------------- */

require_once get_stylesheet_directory() . '/lib/woocommerce/woocommerce-setup.php';
require_once get_stylesheet_directory() . '/lib/woocommerce/woocommerce-output.php';
require_once get_stylesheet_directory() . '/lib/woocommerce/woocommerce-notice.php';

add_action( 'after_setup_theme', 'genesis_child_gutenberg_support' );

/* Adds Gutenberg opt-in features & styling.
-------------------------------------------------------------------------------- */

function genesis_child_gutenberg_support() { // phpcs:ignore WordPress.NamingConventions.PrefixAllGlobals.NonPrefixedFunctionFound -- using same in all child themes to allow action to be unhooked.
	require_once get_stylesheet_directory() . '/lib/gutenberg/init.php';
}

/* Registers the responsive menus.
-------------------------------------------------------------------------------- */

if ( function_exists( 'genesis_register_responsive_menus' ) ) {
	genesis_register_responsive_menus( genesis_get_config( 'responsive-menus' ) );
}

/* Enqueues scripts & styles.
-------------------------------------------------------------------------------- */

add_action( 'wp_enqueue_scripts', 'hkc_enqueue_scripts_styles' );

function hkc_enqueue_scripts_styles() {

	$appearance = genesis_get_config( 'appearance' );

	wp_enqueue_style(
		genesis_get_theme_handle() . '-fonts',
		$appearance['fonts-url'],
		[],
		genesis_get_theme_version()
	);

	wp_enqueue_style( 'dashicons' );

	if ( genesis_is_amp() ) {
		wp_enqueue_style(
			genesis_get_theme_handle() . '-amp',
			get_stylesheet_directory_uri() . '/lib/amp/amp.css',
			[ genesis_get_theme_handle() ],
			genesis_get_theme_version()
		);
	}

	wp_enqueue_style( 'adobe-fonts', 'https://use.typekit.net/bbu8fev.css' );

	// Modal
	wp_enqueue_script( 'jquery-modal', get_stylesheet_directory_uri() . '/lib/js/jquery.modal.min.js', array('jquery') );
	//wp_enqueue_style( 'jquery-modal-css', get_stylesheet_directory_uri() . '/lib/js/jquery.modal.min.css' );
	wp_enqueue_script( 'jquery-script', get_stylesheet_directory_uri() . '/lib/js/script.js', array('jquery') );
	
	// THird party
	wp_enqueue_script('jquery-1', '//code.jquery.com/jquery-3.3.1.min.js', '', '', true);
	wp_enqueue_script('jquery', 'http://ajax.googleapis.com/ajax/libs/jqueryui/1.8/jquery-ui.min.js', '', '', true);
	wp_enqueue_script('jquery-2', 'https://cdnjs.cloudflare.com/ajax/libs/jquery/3.0.0/jquery.min.js', '', '', true);

	// UIKit
	wp_enqueue_script( 'uikit', get_stylesheet_directory_uri() . '/lib/uikit/uikit.min.js', array('jquery') );
	wp_enqueue_script( 'uikit-icons', get_stylesheet_directory_uri() . '/lib/uikit/uikit-icons.min.js', array('jquery') );

	// Compiled stylesheet
	wp_enqueue_style( 'style-general', get_stylesheet_directory_uri() . '/style-general.css' );
	wp_enqueue_style( 'style-test', get_stylesheet_directory_uri() . '/style-test.css' );

}

add_action( 'wp_enqueue_scripts', 'custom_load_font_awesome' );
/**
 * Enqueue Font Awesome.
 */
function custom_load_font_awesome() {

    wp_enqueue_style( 'font-awesome-free', '//use.fontawesome.com/releases/v5.2.0/css/all.css' );

}

/* Enqueues Gutenberg styles and scripts
-------------------------------------------------------------------------------- */
function gutenberg_editor_styles() {
	// UIKit
	wp_enqueue_script( 'uikit', get_stylesheet_directory_uri() . '/lib/uikit/uikit.min.js', array('jquery') );
	wp_enqueue_script( 'uikit-icons', get_stylesheet_directory_uri() . '/lib/uikit/uikit-icons.min.js', array('jquery') );
}
add_action( 'enqueue_block_editor_assets', 'gutenberg_editor_styles' );

/* Add desired theme supports.
 * See config file at `config/theme-supports.php`.
-------------------------------------------------------------------------------- */

add_action( 'after_setup_theme', 'genesis_sample_theme_support', 9 );

function genesis_sample_theme_support() {

	$theme_supports = genesis_get_config( 'theme-supports' );

	foreach ( $theme_supports as $feature => $args ) {
		add_theme_support( $feature, $args );
	}

}

/* Add desired post type supports.
 * See config file at `config/post-type-supports.php`.
-------------------------------------------------------------------------------- */

add_action( 'after_setup_theme', 'genesis_sample_post_type_support', 9 );

function genesis_sample_post_type_support() {

	$post_type_supports = genesis_get_config( 'post-type-supports' );

	foreach ( $post_type_supports as $post_type => $args ) {
		add_post_type_support( $post_type, $args );
	}

}

/* Adds image sizes.
-------------------------------------------------------------------------------- */

add_image_size( 'sidebar-featured', 75, 75, true );
add_image_size( 'genesis-singular-images', 702, 526, true );

/* Removes header right widget area.
-------------------------------------------------------------------------------- */

unregister_sidebar( 'header-right' );

/* Removes secondary sidebar.
-------------------------------------------------------------------------------- */

unregister_sidebar( 'sidebar-alt' );

/* Removes unnecessary site layouts.
-------------------------------------------------------------------------------- */

genesis_unregister_layout( 'content-sidebar-sidebar' );
genesis_unregister_layout( 'sidebar-content-sidebar' );
genesis_unregister_layout( 'sidebar-sidebar-content' );

/* Repositions primary navigation menu.
-------------------------------------------------------------------------------- */

remove_action( 'genesis_after_header', 'genesis_do_nav' );
add_action( 'genesis_header', 'genesis_do_nav', 12 );

/* Repositions secondary navigation menu.
-------------------------------------------------------------------------------- */

remove_action( 'genesis_after_header', 'genesis_do_subnav' );
add_action( 'genesis_footer', 'genesis_do_subnav', 10 );

/*
 * Reduces secondary navigation menu to one level depth.
 * @param array $args Original menu options.
 * @return array Menu options with depth set to 1.
-------------------------------------------------------------------------------- */

add_filter( 'wp_nav_menu_args', 'genesis_sample_secondary_menu_args' );

function genesis_sample_secondary_menu_args( $args ) {

	if ( 'secondary' === $args['theme_location'] ) {
		$args['depth'] = 1;
	}

	return $args;

}

/* Custom colour palette
---------------------------------------------------------------------------- */

add_action( 'after_setup_theme', 'hkc_custom_colors' );

function hkc_custom_colors() {
	add_theme_support( 'editor-color-palette', array(
		array(
			'name'  => __( 'HKC Blue', 'hard-korr-campers' ),
			'slug'  => 'hkc-blue',
			'color'	=> '#008dcf',
		),
		array(
			'name'  => __( 'HKC Dark Blue', 'hard-korr-campers' ),
			'slug'  => 'hkc-dkblue',
			'color' => '#001e33',
		),
		array(
			'name'  => __( 'HKC Orange', 'hard-korr-campers' ),
			'slug'  => 'hkc-orange',
			'color' => '#ff8000',
		),
		array(
			'name'  => __( 'HKC Green', 'hard-korr-campers' ),
			'slug'  => 'hkc-green',
			'color' => '#c4d600',
		),
		array(
			'name'  => __( 'HKC Light Grey', 'hard-korr-campers' ),
			'slug'  => 'hkc-ltgrey',
			'color' => '#dddddd',
		),
		array(
			'name'  => __( 'HKC Dark Gray', 'hard-korr-campers' ),
			'slug'  => 'hkc-dkgrey',
			'color' => '#52524f',
		),
		array(
			'name'  => __( 'HKC Black', 'hard-korr-campers' ),
			'slug'  => 'hkc-black',
			'color' => '#000000',
		),
		array(
			'name'  => __( 'HKC White', 'hard-korr-campers' ),
			'slug'  => 'hkc-white',
			'color' => '#ffffff',
		),
	) );
}

/* Remove footer
---------------------------------------------------------------------------- */
//remove_action('genesis_footer', 'genesis_do_footer'); //In footer.php
remove_action('genesis_footer', 'genesis_footer_markup_open', 5);
remove_action('genesis_footer', 'genesis_footer_markup_close', 15);

/* Remove default header and add new header
---------------------------------------------------------------------------- */
require_once get_stylesheet_directory() . '/lib/header.php';

/* Remove default footer and add new footer
---------------------------------------------------------------------------- */
require_once get_stylesheet_directory() . '/lib/footer.php';

/* WooCommerce
---------------------------------------------------------------------------- */
add_filter( 'woocommerce_product_tabs', 'misha_custom_tab' );
 
function misha_custom_tab( $tabs ) {

	global $product;
 
	if( $product->is_type( 'variable' ) ) {
	$tabs['misha_custom_tab'] = array(
		'title'    => 'About Misha',
		'callback' => 'misha_custom_tab_content', // the function name, which is on line 15
		'priority' => 50,
	);
 
	return $tabs;
}
}
 
function misha_custom_tab_content( $slug, $tab ) {
 
	echo '<h2>' . $tab['title'] . '</h2><p>Tab Content. You can display something in PHP here as well.</p>';
 
}


















// Product Page
remove_action( 'woocommerce_before_main_content', 'woocommerce_breadcrumb', 20, 0 );
remove_action( 'woocommerce_before_single_product_summary', 'woocommerce_show_product_sale_flash', 10 );

/* ACF Blocks
---------------------------------------------------------------------------- */

function register_acf_block_types() {
    acf_register_block_type(array(
        'name'              => 'campers-intro',
        'title'             => __('Campers Intro'),
        'render_template'   => 'template-parts/blocks/campers-intro.php',
		'category'          => 'layout',
		'icon' => '<svg aria-hidden="true" focusable="false" data-prefix="fas" data-icon="network-wired" class="svg-inline--fa fa-network-wired fa-w-20" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 640 512"><path fill="currentColor" d="M640 264v-16c0-8.84-7.16-16-16-16H344v-40h72c17.67 0 32-14.33 32-32V32c0-17.67-14.33-32-32-32H224c-17.67 0-32 14.33-32 32v128c0 17.67 14.33 32 32 32h72v40H16c-8.84 0-16 7.16-16 16v16c0 8.84 7.16 16 16 16h104v40H64c-17.67 0-32 14.33-32 32v128c0 17.67 14.33 32 32 32h160c17.67 0 32-14.33 32-32V352c0-17.67-14.33-32-32-32h-56v-40h304v40h-56c-17.67 0-32 14.33-32 32v128c0 17.67 14.33 32 32 32h160c17.67 0 32-14.33 32-32V352c0-17.67-14.33-32-32-32h-56v-40h104c8.84 0 16-7.16 16-16zM256 128V64h128v64H256zm-64 320H96v-64h96v64zm352 0h-96v-64h96v64z"></path></svg>',
		'mode' => 'auto'
	));
		acf_register_block_type(array(
			'name'              => 'why-hkc',
			'title'             => __('Why Hard Korr Campers'),
			'render_template'   => 'template-parts/blocks/why-hkc.php',
			'category'          => 'layout',
			'icon' => '<svg aria-hidden="true" focusable="false" data-prefix="fas" data-icon="analytics" class="svg-inline--fa fa-analytics fa-w-18" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 576 512"><path fill="currentColor" d="M510.62 92.63C516.03 94.74 521.85 96 528 96c26.51 0 48-21.49 48-48S554.51 0 528 0s-48 21.49-48 48c0 2.43.37 4.76.71 7.09l-95.34 76.27c-5.4-2.11-11.23-3.37-17.38-3.37s-11.97 1.26-17.38 3.37L255.29 55.1c.35-2.33.71-4.67.71-7.1 0-26.51-21.49-48-48-48s-48 21.49-48 48c0 4.27.74 8.34 1.78 12.28l-101.5 101.5C56.34 160.74 52.27 160 48 160c-26.51 0-48 21.49-48 48s21.49 48 48 48 48-21.49 48-48c0-4.27-.74-8.34-1.78-12.28l101.5-101.5C199.66 95.26 203.73 96 208 96c6.15 0 11.97-1.26 17.38-3.37l95.34 76.27c-.35 2.33-.71 4.67-.71 7.1 0 26.51 21.49 48 48 48s48-21.49 48-48c0-2.43-.37-4.76-.71-7.09l95.32-76.28zM400 320h-64c-8.84 0-16 7.16-16 16v160c0 8.84 7.16 16 16 16h64c8.84 0 16-7.16 16-16V336c0-8.84-7.16-16-16-16zm160-128h-64c-8.84 0-16 7.16-16 16v288c0 8.84 7.16 16 16 16h64c8.84 0 16-7.16 16-16V208c0-8.84-7.16-16-16-16zm-320 0h-64c-8.84 0-16 7.16-16 16v288c0 8.84 7.16 16 16 16h64c8.84 0 16-7.16 16-16V208c0-8.84-7.16-16-16-16zM80 352H16c-8.84 0-16 7.16-16 16v128c0 8.84 7.16 16 16 16h64c8.84 0 16-7.16 16-16V368c0-8.84-7.16-16-16-16z"></path></svg>',
			'mode' => 'auto'
		));
		acf_register_block_type(array(
			'name'              => 'find-dealer',
			'title'             => __('Find A Dealer'),
			'render_template'   => 'template-parts/blocks/find-dealer.php',
			'category'          => 'layout',
			'icon' => '<svg aria-hidden="true" focusable="false" data-prefix="fad" data-icon="user-tie" class="svg-inline--fa fa-user-tie fa-w-14" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512"><g class="fa-group"><path class="fa-secondary" fill="currentColor" d="M191.35 414.77L208 344l-32-56h96l-32 56 16.65 70.77L224 480zM224 256A128 128 0 1 0 96 128a128 128 0 0 0 128 128z" opacity="0.4"></path><path class="fa-primary" fill="currentColor" d="M319.8 288.6L224 480l-95.8-191.4C56.9 292 0 350.3 0 422.4V464a48 48 0 0 0 48 48h352a48 48 0 0 0 48-48v-41.6c0-72.1-56.9-130.4-128.2-133.8z"></path></g></svg>',
			'mode' => 'auto'
		));
		acf_register_block_type(array(
			'name'              => 'chevron',
			'title'             => __('Chevron'),
			'render_template'   => 'template-parts/blocks/chevron.php',
			'category'          => 'layout',
			'icon' => '<svg aria-hidden="true" focusable="false" data-prefix="fad" data-icon="user-tie" class="svg-inline--fa fa-user-tie fa-w-14" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512"><g class="fa-group"><path class="fa-secondary" fill="currentColor" d="M191.35 414.77L208 344l-32-56h96l-32 56 16.65 70.77L224 480zM224 256A128 128 0 1 0 96 128a128 128 0 0 0 128 128z" opacity="0.4"></path><path class="fa-primary" fill="currentColor" d="M319.8 288.6L224 480l-95.8-191.4C56.9 292 0 350.3 0 422.4V464a48 48 0 0 0 48 48h352a48 48 0 0 0 48-48v-41.6c0-72.1-56.9-130.4-128.2-133.8z"></path></g></svg>',
			'mode' => 'auto'
		));
		acf_register_block_type(array(
			'name'              => 'events-calendar-block',
			'title'             => __('Events Calendar'),
			'render_template'   => 'template-parts/blocks/events-calendar.php',
			'category'          => 'layout',
			'icon' => '<svg aria-hidden="true" focusable="false" data-prefix="fad" data-icon="user-tie" class="svg-inline--fa fa-user-tie fa-w-14" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512"><g class="fa-group"><path class="fa-secondary" fill="currentColor" d="M191.35 414.77L208 344l-32-56h96l-32 56 16.65 70.77L224 480zM224 256A128 128 0 1 0 96 128a128 128 0 0 0 128 128z" opacity="0.4"></path><path class="fa-primary" fill="currentColor" d="M319.8 288.6L224 480l-95.8-191.4C56.9 292 0 350.3 0 422.4V464a48 48 0 0 0 48 48h352a48 48 0 0 0 48-48v-41.6c0-72.1-56.9-130.4-128.2-133.8z"></path></g></svg>',
			'mode' => 'auto'
		));
		acf_register_block_type(array(
			'name'              => 'camper-overview',
			'title'             => __('Camper Overview'),
			'render_template'   => 'template-parts/blocks/camper-overview.php',
			'category'          => 'layout',
			'icon' => '<svg aria-hidden="true" focusable="false" data-prefix="fad" data-icon="trailer" class="svg-inline--fa fa-trailer fa-w-20" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 640 512"><g class="fa-group"><path class="fa-secondary" fill="currentColor" d="M176,320a80,80,0,1,0,80,80A80,80,0,0,0,176,320ZM96,136a8,8,0,0,0-8-8H72a8,8,0,0,0-8,8V264.39a176.29,176.29,0,0,1,32-20.71Zm96,89.14V136a8,8,0,0,0-8-8H168a8,8,0,0,0-8,8v89.14c5.31-.49,10.57-1.14,16-1.14S186.69,224.65,192,225.14ZM280,128H264a8,8,0,0,0-8,8V243.68a176.29,176.29,0,0,1,32,20.71V136A8,8,0,0,0,280,128Zm192,0H456a8,8,0,0,0-8,8V320h32V136A8,8,0,0,0,472,128Zm-96,0H360a8,8,0,0,0-8,8V320h32V136A8,8,0,0,0,376,128Z" opacity="0.4"></path><path class="fa-primary" fill="currentColor" d="M624,320H544V80a16,16,0,0,0-16-16H16A16,16,0,0,0,0,80V368a16,16,0,0,0,16,16H65.61c7.83-54.21,54-96,110.39-96s102.56,41.79,110.39,96H624a16,16,0,0,0,16-16V336A16,16,0,0,0,624,320ZM96,243.68a176.29,176.29,0,0,0-32,20.71V136a8,8,0,0,1,8-8H88a8,8,0,0,1,8,8Zm96-18.54c-5.31-.49-10.57-1.14-16-1.14s-10.69.65-16,1.14V136a8,8,0,0,1,8-8h16a8,8,0,0,1,8,8Zm96,39.25a176.29,176.29,0,0,0-32-20.71V136a8,8,0,0,1,8-8h16a8,8,0,0,1,8,8ZM384,320H352V136a8,8,0,0,1,8-8h16a8,8,0,0,1,8,8Zm96,0H448V136a8,8,0,0,1,8-8h16a8,8,0,0,1,8,8Z"></path></g></svg>',
			'mode' => 'auto'
		));
		acf_register_block_type(array(
			'name'              => 'slider-syncing',
			'title'             => __('Slider Syncing'),
			'render_template'   => 'template-parts/blocks/slider-syncing.php',
			'category'          => 'layout',
			'icon' => '<svg aria-hidden="true" focusable="false" data-prefix="fas" data-icon="images" class="svg-inline--fa fa-images fa-w-18" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 576 512"><path fill="currentColor" d="M480 416v16c0 26.51-21.49 48-48 48H48c-26.51 0-48-21.49-48-48V176c0-26.51 21.49-48 48-48h16v208c0 44.112 35.888 80 80 80h336zm96-80V80c0-26.51-21.49-48-48-48H144c-26.51 0-48 21.49-48 48v256c0 26.51 21.49 48 48 48h384c26.51 0 48-21.49 48-48zM256 128c0 26.51-21.49 48-48 48s-48-21.49-48-48 21.49-48 48-48 48 21.49 48 48zm-96 144l55.515-55.515c4.686-4.686 12.284-4.686 16.971 0L272 256l135.515-135.515c4.686-4.686 12.284-4.686 16.971 0L512 208v112H160v-48z"></path></svg>',
			'mode' => 'auto'
		));
		acf_register_block_type(array(
			'name'              => 'camper-description',
			'title'             => __('Camper Description'),
			'render_template'   => 'template-parts/blocks/camper-description.php',
			'category'          => 'layout',
			'icon' => '<svg aria-hidden="true" focusable="false" data-prefix="fas" data-icon="file-alt" class="svg-inline--fa fa-file-alt fa-w-12" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 384 512"><path fill="currentColor" d="M224 136V0H24C10.7 0 0 10.7 0 24v464c0 13.3 10.7 24 24 24h336c13.3 0 24-10.7 24-24V160H248c-13.2 0-24-10.8-24-24zm64 236c0 6.6-5.4 12-12 12H108c-6.6 0-12-5.4-12-12v-8c0-6.6 5.4-12 12-12h168c6.6 0 12 5.4 12 12v8zm0-64c0 6.6-5.4 12-12 12H108c-6.6 0-12-5.4-12-12v-8c0-6.6 5.4-12 12-12h168c6.6 0 12 5.4 12 12v8zm0-72v8c0 6.6-5.4 12-12 12H108c-6.6 0-12-5.4-12-12v-8c0-6.6 5.4-12 12-12h168c6.6 0 12 5.4 12 12zm96-114.1v6.1H256V0h6.1c6.4 0 12.5 2.5 17 7l97.9 98c4.5 4.5 7 10.6 7 16.9z"></path></svg>',
			'mode' => 'auto'
		));
}

// Check if function exists and hook into setup.
if( function_exists('acf_register_block_type') ) {
    add_action('acf/init', 'register_acf_block_types');
}