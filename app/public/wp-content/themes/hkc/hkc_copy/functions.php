<?php
/**
 * Hard Korr Campers.
 *
 * This file adds functions to the Hard Korr Campers Theme.
 *
 * @package Hard Korr Campers
 * @author  Mack Marketing
 * @license GPL-2.0-or-later
 * @link    https://mackmarketing.com.au
 */

/* Starts the engine
-------------------------------------------------------------------------------- */

require_once get_template_directory() . '/lib/init.php';

/* Sets up the theme
-------------------------------------------------------------------------------- */

require_once get_stylesheet_directory() . '/lib/theme-defaults.php';

/* Sets localisation (do not remove)
-------------------------------------------------------------------------------- */

add_action( 'after_setup_theme', 'genesis_sample_localization_setup' );

function genesis_sample_localization_setup() {

	load_child_theme_textdomain( genesis_get_theme_handle(), get_stylesheet_directory() . '/languages' );

}

/* Adds helper functions
-------------------------------------------------------------------------------- */

require_once get_stylesheet_directory() . '/lib/helper-functions.php';

/* Adds image upload and colour select to customiser.
-------------------------------------------------------------------------------- */

require_once get_stylesheet_directory() . '/lib/customize.php';

/* Includes customiser CSS.
-------------------------------------------------------------------------------- */

require_once get_stylesheet_directory() . '/lib/output.php';

/* Adds WooCommerce support.
-------------------------------------------------------------------------------- */

require_once get_stylesheet_directory() . '/lib/woocommerce/woocommerce-setup.php';
require_once get_stylesheet_directory() . '/lib/woocommerce/woocommerce-output.php';
require_once get_stylesheet_directory() . '/lib/woocommerce/woocommerce-notice.php';

add_action( 'after_setup_theme', 'genesis_child_gutenberg_support' );

/* Adds Gutenberg opt-in features & styling.
-------------------------------------------------------------------------------- */

function genesis_child_gutenberg_support() { // phpcs:ignore WordPress.NamingConventions.PrefixAllGlobals.NonPrefixedFunctionFound -- using same in all child themes to allow action to be unhooked.
	require_once get_stylesheet_directory() . '/lib/gutenberg/init.php';
}

/* Registers the responsive menus.
-------------------------------------------------------------------------------- */

if ( function_exists( 'genesis_register_responsive_menus' ) ) {
	genesis_register_responsive_menus( genesis_get_config( 'responsive-menus' ) );
}

/* Enqueues scripts & styles.
-------------------------------------------------------------------------------- */

add_action( 'wp_enqueue_scripts', 'hkc_enqueue_scripts_styles' );

function hkc_enqueue_scripts_styles() {

	$appearance = genesis_get_config( 'appearance' );

	wp_enqueue_style(
		genesis_get_theme_handle() . '-fonts',
		$appearance['fonts-url'],
		[],
		genesis_get_theme_version()
	);

	wp_enqueue_style( 'dashicons' );

	if ( genesis_is_amp() ) {
		wp_enqueue_style(
			genesis_get_theme_handle() . '-amp',
			get_stylesheet_directory_uri() . '/lib/amp/amp.css',
			[ genesis_get_theme_handle() ],
			genesis_get_theme_version()
		);
	}

	wp_enqueue_style( 'adobe-fonts', 'https://use.typekit.net/bbu8fev.css' );

	// Modal
	wp_enqueue_script( 'jquery-modal', get_stylesheet_directory_uri() . '/lib/js/jquery.modal.min.js', array('jquery') );
	wp_enqueue_style( 'jquery-modal-css', get_stylesheet_directory_uri() . '/lib/js/jquery.modal.min.css' );

	// UIKit
	wp_enqueue_script( 'uikit', get_stylesheet_directory_uri() . '/lib/uikit/uikit.min.js', array('jquery') );
	wp_enqueue_script( 'uikit-icons', get_stylesheet_directory_uri() . '/lib/uikit/uikit-icons.min.js', array('jquery') );

	// Compiled stylesheet
	wp_enqueue_style( 'style-general', get_stylesheet_directory_uri() . '/style-general.css' );

}

/* Add desired theme supports.
 * See config file at `config/theme-supports.php`.
-------------------------------------------------------------------------------- */

add_action( 'after_setup_theme', 'genesis_sample_theme_support', 9 );

function genesis_sample_theme_support() {

	$theme_supports = genesis_get_config( 'theme-supports' );

	foreach ( $theme_supports as $feature => $args ) {
		add_theme_support( $feature, $args );
	}

}

/* Add desired post type supports.
 * See config file at `config/post-type-supports.php`.
-------------------------------------------------------------------------------- */

add_action( 'after_setup_theme', 'genesis_sample_post_type_support', 9 );

function genesis_sample_post_type_support() {

	$post_type_supports = genesis_get_config( 'post-type-supports' );

	foreach ( $post_type_supports as $post_type => $args ) {
		add_post_type_support( $post_type, $args );
	}

}

/* Adds image sizes.
-------------------------------------------------------------------------------- */

add_image_size( 'sidebar-featured', 75, 75, true );
add_image_size( 'genesis-singular-images', 702, 526, true );

/* Removes header right widget area.
-------------------------------------------------------------------------------- */

unregister_sidebar( 'header-right' );

/* Removes secondary sidebar.
-------------------------------------------------------------------------------- */

unregister_sidebar( 'sidebar-alt' );

/* Removes unnecessary site layouts.
-------------------------------------------------------------------------------- */

genesis_unregister_layout( 'content-sidebar-sidebar' );
genesis_unregister_layout( 'sidebar-content-sidebar' );
genesis_unregister_layout( 'sidebar-sidebar-content' );

/* Repositions primary navigation menu.
-------------------------------------------------------------------------------- */

remove_action( 'genesis_after_header', 'genesis_do_nav' );
add_action( 'genesis_header', 'genesis_do_nav', 12 );

/* Repositions secondary navigation menu.
-------------------------------------------------------------------------------- */

remove_action( 'genesis_after_header', 'genesis_do_subnav' );
add_action( 'genesis_footer', 'genesis_do_subnav', 10 );

/*
 * Reduces secondary navigation menu to one level depth.
 * @param array $args Original menu options.
 * @return array Menu options with depth set to 1.
-------------------------------------------------------------------------------- */

add_filter( 'wp_nav_menu_args', 'genesis_sample_secondary_menu_args' );

function genesis_sample_secondary_menu_args( $args ) {

	if ( 'secondary' === $args['theme_location'] ) {
		$args['depth'] = 1;
	}

	return $args;

}

/* Custom colour palette
---------------------------------------------------------------------------- */

add_action( 'after_setup_theme', 'hkc_custom_colors' );

function hkc_custom_colors() {
	add_theme_support( 'editor-color-palette', array(
		array(
			'name'  => __( 'HKC Blue', 'hard-korr-campers' ),
			'slug'  => 'hkc-blue',
			'color'	=> '#008dcf',
		),
		array(
			'name'  => __( 'HKC Dark Blue', 'hard-korr-campers' ),
			'slug'  => 'hkc-dkblue',
			'color' => '#001e33',
		),
		array(
			'name'  => __( 'HKC Orange', 'hard-korr-campers' ),
			'slug'  => 'hkc-orange',
			'color' => '#ff8000',
		),
		array(
			'name'  => __( 'HKC Green', 'hard-korr-campers' ),
			'slug'  => 'hkc-green',
			'color' => '#c4d600',
		),
		array(
			'name'  => __( 'HKC Light Grey', 'hard-korr-campers' ),
			'slug'  => 'hkc-ltgrey',
			'color' => '#dddddd',
		),
		array(
			'name'  => __( 'HKC Dark Gray', 'hard-korr-campers' ),
			'slug'  => 'hkc-dkgrey',
			'color' => '#52524f',
		),
		array(
			'name'  => __( 'HKC Black', 'hard-korr-campers' ),
			'slug'  => 'hkc-black',
			'color' => '#000000',
		),
		array(
			'name'  => __( 'HKC White', 'hard-korr-campers' ),
			'slug'  => 'hkc-white',
			'color' => '#ffffff',
		),
	) );
}

/* Remove footer
---------------------------------------------------------------------------- */

remove_action('genesis_footer', 'genesis_do_footer');
remove_action('genesis_footer', 'genesis_footer_markup_open', 5);
remove_action('genesis_footer', 'genesis_footer_markup_close', 15);