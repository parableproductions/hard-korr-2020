<?php

/**
 * why-hkc Block Template.
 *
 * @param   array $block The block settings and attributes.
 * @param   string $content The block inner HTML (empty).
 * @param   bool $is_preview True during AJAX preview.
 * @param   (int|string) $post_id The post ID this block is saved to.
 */

// Create id attribute allowing for custom "anchor" value.
$id = 'events-calendar-' . $block['id'];
if( !empty($block['anchor']) ) {
    $id = $block['anchor'];
}

// Create class attribute allowing for custom "className" and "align" values.
$className = 'events-calendar';
if( !empty($block['className']) ) {
    $className .= ' ' . $block['className'];
}
if( !empty($block['align']) ) {
    $className .= ' align' . $block['align'];
} ?>

<div id="<?php echo esc_attr($id); ?>" class="<?php echo esc_attr($className); ?>">

<?php
// Retrieve the next 5 upcoming events
$events = tribe_get_events( [
    'posts_per_page' => 3,
    'start_date' => '2015-01-01',
 ] );

// Loop through the events, displaying the title and content for each
foreach ( $events as $event ) { ?>


<?php echo $event->post_title; ?>
<?php $featured_image = tribe_event_featured_image(null, 'thumbnails-medium'); ?>
<?php 
if (empty($featured_image)) {
    echo 'no-image';
}
?>

<?php } ?>


</div>