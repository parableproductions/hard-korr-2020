<?php

/**
 * find-dealer Block Template.
 *
 * @param   array $block The block settings and attributes.
 * @param   string $content The block inner HTML (empty).
 * @param   bool $is_preview True during AJAX preview.
 * @param   (int|string) $post_id The post ID this block is saved to.
 */

// Create id attribute allowing for custom "anchor" value.
$id = 'find-dealer-' . $block['id'];
if( !empty($block['anchor']) ) {
    $id = $block['anchor'];
}

// Create class attribute allowing for custom "className" and "align" values.
$className = 'find-dealer';
if( !empty($block['className']) ) {
    $className .= ' ' . $block['className'];
}
if( !empty($block['align']) ) {
    $className .= ' align' . $block['align'];
}

// Load values and assign defaults.
?>
<!--<?php get_template_part("template-parts/template-showevent"); ?>-->
<div id="<?php echo $id; ?>" class="<?php echo $className; ?>">
    <?php    
    $image = get_field('image');
    $title = get_field('title');
    $content = get_field('content'); 
    $finddealer = get_field('find_dealer');    
   
    ?>

    <div class="dealer-container" style="background-image: url('<?php echo esc_url($image['url']); ?>'); background-repeat: no-repeat; background-size: cover; background-attachment: fixed;">
        <div class="overlay-black"></div>
            <div class="uk-container pad-to-bot">
                <div class="wrap">
                    <div class="wrap__left">
                    <h1><?php echo $title; ?></h1>
                    </div>
                    <div class="wrap__right">
                        <div class="right_content">
                            <?php echo $content; ?>
                        </div>
                        <a href="http://hard-korr-campers-2020.local/dealer-location/" target="_blank"> <div class="wrapper-map">
                            <?php echo $finddealer; ?>
                        </div></a>
                    </div>
                </div>
                <div class="white-strip"></div>
            </div>
            <div class="bottom-chevron">
                <img src="http://hard-korr-campers-2020.local/wp-content/uploads/chevron-bottom.svg" width="100%">
            
            </div>
        </div>
</div>


