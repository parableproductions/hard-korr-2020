<?php

/**
 * campers-intro Block Template.
 *
 * @param   array $block The block settings and attributes.
 * @param   string $content The block inner HTML (empty).
 * @param   bool $is_preview True during AJAX preview.
 * @param   (int|string) $post_id The post ID this block is saved to.
 */

// Create id attribute allowing for custom "anchor" value.
$id = 'campers-intro-' . $block['id'];
if( !empty($block['anchor']) ) {
    $id = $block['anchor'];
}

// Create class attribute allowing for custom "className" and "align" values.
$className = 'campers-intro';
if( !empty($block['className']) ) {
    $className .= ' ' . $block['className'];
}
if( !empty($block['align']) ) {
    $className .= ' align' . $block['align'];
} ?>
<div id="<?php echo esc_attr($id); ?>" class="<?php echo esc_attr($className); ?>">
    <!-- Slider -->
    <?php if( have_rows('campers_introduction') ): ?>
    <div class="uk-container">
    <div class="uk-slidenav-position" data-uk-slider>
        <div uk-slider="finite: true;">
            <div class="uk-slider-items uk-child-width-1-4 uk-grid" uk-switcher>
                <?php while ( have_rows( 'campers_introduction' ) ) : the_row(); 
                $image = get_sub_field('camper_img');
                $title = get_sub_field('camper_title');
                $subtitle = get_sub_field('camper_subtitle');
                $content = get_sub_field('camper_content');
                ?>
                <div>
                    <a href="#">
                        <!-- Max with set to  230px-->
                        <div class="image-wrap">
                             <img class="campers-intro-image" src="<?php echo esc_url($image['url']); ?>"alt="<?php echo esc_attr($image['alt']); ?>">
                        </div>
                        <div class="campers-intro-caption">
                            <div class="campers-intro-caption-bg">
                                <div class="campers-intro-title"><h4><?php echo $title; ?></h4></div>
                                <div class="campers-intro-subtitle"><h4 class="light"><?php echo $subtitle; ?><h4></div>
                            </div>
                            <div class="active-chevron"></div>
                        </div>
                    </a>
                </div>
                <?php endwhile; ?>
                
            </div>
            <hr class="grey-line">
            <div class="uk-switcher">
                <?php while ( have_rows( 'campers_introduction' ) ) : the_row(); 
                $image = get_sub_field('camper_img');
                $logo = get_sub_field('camper_logo');
                $content = get_sub_field('camper_content');
                ?>
                <div>
                    <div class="uk-grid-large uk-child-width-expand@l uk-grid uk-grid-stack">
                        <div class="large-image">
                            <img class="large-image__thumbnail" src="<?php echo esc_url($image['url']); ?>"alt="<?php echo esc_attr($image['alt']); ?>">
                        </div>
                        <div>
                            <div class="logo-image">
                                <img class="logo-image__thumbnail" src="<?php echo esc_url($logo['url']); ?>"alt="<?php echo esc_attr($logo['alt']); ?>">
                            </div>
                            <div class="campers-intro-content">
                                <h4><?php echo $content; ?></h4>
                            </div>
                            <div class="campers-intro-buttons">
                                <button class="uk-button uk-button-black">FIND OUT MORE<span uk-icon="chevron-double-right"></span></i></button>
                                <a href="#contact-model" rel="modal:open"><button class="uk-button uk-button-orange">ENQUIRE NOW<span uk-icon="chevron-double-right"></button></a>
                            </div>
                        </div>
                    </div>
                </div>
                <?php endwhile; ?>
            </div>
            <div class="uk-slider-nav uk-dotnav uk-flex-center"></div>
        </div>

       <!-- <div class="uk-hidden@s">
            <a class="uk-position-center-left" href="#" uk-slidenav-previous uk-slider-item="previous"></a>
            <a class="uk-position-center-right" href="#" uk-slidenav-next uk-slider-item="next"></a>
        </div>

        <div class="uk-visible@s">
            <a class="uk-position-center-left" href="#" uk-slidenav-previous uk-slider-item="previous"></a>
            <a class="uk-position-center-right" href="#" uk-slidenav-next uk-slider-item="next"></a>
        </div>-->

        <a class="uk-position-center-left" href="#" uk-slider-item="previous"></a>
        <a class="uk-position-center-right" href="#" uk-slider-item="next"></a>

    </div>
</div>
<!--<div class="side-butto-wrap">
    <a href="#" class="side-butto-wrap__btn uk-button uk-button-orange">ENQUIRE NOW</a>
</div>-->
    
    <?php endif; ?>
    <!-- /Slider -->
</div>
<div class="side-button-wrap">
    <a href="#contact-model" rel="modal:open" class="side-butto-wrap__btn uk-button uk-button-orange">ENQUIRE NOW</a>
</div>


<!-- Modal HTML embedded directly into document -->
<div id="contact-model" class="modal">
    <?= do_shortcode('[contact-form-7 id="653" title="Contact Us"]'); ?>
</div>



<!-- old slider -->

<!-- <div id="<?php echo esc_attr($id); ?>" class="<?php echo esc_attr($className); ?>">
    <!-- Slider
    <?php if( have_rows('campers_introduction') ): ?>
    <div class="uk-container">
        <div uk-slider="finite: true;">
            <ul class="uk-slider-items uk-child-width-1-4 uk-grid" uk-switcher>
                <?php while ( have_rows( 'campers_introduction' ) ) : the_row(); 
                $image = get_sub_field('camper_img');
                $title = get_sub_field('camper_title');
                $subtitle = get_sub_field('camper_subtitle');
                $content = get_sub_field('camper_content');
                ?>
                <li>
                    <a href="#">
                        <!-- Max with set to  230px
                        <img class="campers-intro-image" src="<?php echo esc_url($image['url']); ?>"
                            alt="<?php echo esc_attr($image['alt']); ?>">
                        <div class="campers-intro-caption">
                            <div class="campers-intro-caption-bg">
                                <div class="campers-intro-title"><?php echo $title; ?></div>
                                <div class="campers-intro-subtitle"><?php echo $subtitle; ?></div>
                            </div>
                            <div class="active-chevron"></div>
                        </div>
                    </a>
                </li>
                <?php endwhile; ?>
            </ul>
            <hr class="grey-line">
            <ul class="uk-switcher">
                <?php while ( have_rows( 'campers_introduction' ) ) : the_row(); 
                $image = get_sub_field('camper_img');
                $logo = get_sub_field('camper_logo');
                $content = get_sub_field('camper_content');
                ?>
                <li>
                    <div class="uk-grid-large uk-child-width-expand@l uk-grid uk-grid-stack">
                        <div>
                            <img src="<?php echo esc_url($image['url']); ?>"
                                alt="<?php echo esc_attr($image['alt']); ?>">
                        </div>
                        <div>
                            <img class="campers-intro-image" src="<?php echo esc_url($logo['url']); ?>"
                                alt="<?php echo esc_attr($logo['alt']); ?>">
                            <div class="campers-intro-content"><?php echo $content; ?></div>
                            <div class="campers-intro-buttons">
                                <button class="uk-button uk-button-black">FIND OUT MORE<span uk-icon="chevron-double-right"></span></i></button>
                                <button class="uk-button uk-button-orange">ENQUIRE NOW<span uk-icon="chevron-double-right"></button>
                                
                            </div>
                        </div>
                    </div>
                </li>
                <?php endwhile; ?>
            </ul>

            <ul class="uk-slider-nav uk-dotnav uk-flex-center"></ul>
            
        </div>
    </div>
    <?php endif; ?>
    <!-- /Slider -->
</div>

<!-- end old slider -->