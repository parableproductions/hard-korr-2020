<?php

/**
 * camper-Overview Block Template.
 *
 * @param   array $block The block settings and attributes.
 * @param   string $content The block inner HTML (empty).
 * @param   bool $is_preview True during AJAX preview.
 * @param   (int|string) $post_id The post ID this block is saved to.
 */

// Create id attribute allowing for custom "anchor" value.
$id = 'camper-overview-' . $block['id'];
if( !empty($block['anchor']) ) {
    $id = $block['anchor'];
}

// Create class attribute allowing for custom "className" and "align" values.
$className = 'camper-overview';
if( !empty($block['className']) ) {
    $className .= ' ' . $block['className'];
}
if( !empty($block['align']) ) {
    $className .= ' align' . $block['align'];
} ?>
<div id="<?php echo $id; ?>" class="<?php echo $className; ?>">
    <?php 
    $title = get_field('camper_title'); 
    $priceOld = get_field('camper_price_old');
    $price = get_field('camper_price');
    $intro = get_field('camper_intro');

    ?>
    <div class="uk-container">
        <div class="uk-grid">
            <div class="uk-width-1-2">
                <h2 class="has-hkc-black-color has-text-color"><?php echo $title; ?> </h2>
                <?php echo $priceOld; ?>
                <?php echo $price;?>
            </div>
            <div class="uk-width-1-2 bg-grey">
                <?php echo $intro; ?>
            </div>
        </div>

        <div class="uk-grid">

            <?php if( have_rows('camper_info') ):
        while( have_rows('camper_info') ): the_row();
        $info_title = get_sub_field('info_title');
        $info_icon = get_sub_field('info_icon');
        $info_text = get_sub_field('info_text');
?>
            <div class="uk-width-1-6 uk-text-center">
                <img class="info_icon" src="<?php echo esc_url($info_icon['url']); ?>" alt="<?php echo esc_attr($info_icon['alt']); ?>">
                <h5 class="info-title"><?php echo $info_title; ?></h5>
                <p class="info-text"><?php echo $info_text; ?></p>
            </div>
            <? endwhile; ?>
            <?php endif; ?>
        </div>


    </div>