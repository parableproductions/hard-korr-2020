<?php

/**
 * why-hkc Block Template.
 *
 * @param   array $block The block settings and attributes.
 * @param   string $content The block inner HTML (empty).
 * @param   bool $is_preview True during AJAX preview.
 * @param   (int|string) $post_id The post ID this block is saved to.
 */

// Create id attribute allowing for custom "anchor" value.
$id = 'why-hkc-' . $block['id'];
if( !empty($block['anchor']) ) {
    $id = $block['anchor'];
}

// Create class attribute allowing for custom "className" and "align" values.
$className = 'why-hkc';
if( !empty($block['className']) ) {
    $className .= ' ' . $block['className'];
}
if( !empty($block['align']) ) {
    $className .= ' align' . $block['align'];
}

// Load values and assign defaults.
if( have_rows('why_hardkorrcamper') ):
?>

<div id="<?php echo esc_attr($id); ?>" class="<?php echo esc_attr($className); ?>">
    <div class="uk-container">
        <div class="uk-child-width-expand@l uk-grid uk-grid-stack">
            <?php while ( have_rows( 'why_hardkorrcamper' ) ) : the_row(); 
                $tagline = get_sub_field('tagline');
                $title = get_sub_field('title');
                $content = get_sub_field('content');    
                $image = get_sub_field('image');
                $image_hover = get_sub_field('image_hover');
                ?>

                    <div class="uk-grid mt-0">
                        <div class="uk-width-4-4 no-pad">
                            <div class="whyhkc-content-wrapper">
                            <div class="dark-overlay"></div>
                            <img class="swap-on-hover__front-image" src="<?php echo esc_url($image['url']); ?>" alt="<?php echo esc_attr($image['alt']); ?>"/>
                                <h6><?php echo $tagline; ?></h6>
                                <h5><?php echo $title; ?></h5>
                                <p><?php echo $content; ?></p>
                            </div>
                        </div>
                    </div>

            <?php endwhile; ?>
        </div>
    </div>
    <?php endif; ?>
</div>


    