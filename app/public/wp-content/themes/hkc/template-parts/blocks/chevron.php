<?php

/**
 * Chevron Block Template.
 *
 * @param   array $block The block settings and attributes.
 * @param   string $content The block inner HTML (empty).
 * @param   bool $is_preview True during AJAX preview.
 * @param   (int|string) $post_id The post ID this block is saved to.
 */

// Create id attribute allowing for custom "anchor" value.
$id = 'chevron' . $block['id'];
if( !empty($block['anchor']) ) {
    $id = $block['anchor'];
}

// Create class attribute allowing for custom "className" and "align" values.
$className = 'chevron';
if( !empty($block['className']) ) {
    $className .= ' ' . $block['className'];
}
if( !empty($block['align']) ) {
    $className .= ' align' . $block['align'];
} ?>
<div id="<?php echo $id; ?>" class="<?php echo $className; ?>">
    <?php 
    $image = get_field('chevron_image'); 
    $logo = get_field('chevron_logo');
    $title = get_field('chevron_title');
    $content = get_field('chevron_content');
    ?>

    
    <div class="chevron-wrapper" style="background-image: url('<?php echo esc_url($image['url']); ?>'); background-repeat: no-repeat; background-size: cover; background-attachment: fixed;">
    
       <div class="uk-container-custom pad-to-bot">
         <div class="overlay-bg">
            <!--<div class="uk-grid-large uk-grid uk-child-width-expand@s uk-text-center">-->
                <div class="wrap">
                    <div class="wrap__left">
                        <div class="title-text">
                            <h1>BATTLE<br/>TESTED.</h1>
                            offroad-logo.png
                        </div>
                        <div class="bottom-logo">
                            <img src="http://localhost:10003/wp-content/uploads/offroad-logo.png" width="418">
                            
                        </div>
                    </div>
                    <div class="wrap__right">
                        <div class="right-text">
                            <h5>We’re not bragging when we say our Hard Korr camper trailers are basically unbreakable.</h5>
                            <p>In collaboration with The Offroad Adventure Show, we’ve tested their absolute limits along twisting, overgrown, barely navigable 4wd tracks, where deep ruts, deep water and deep drop-offs are always around the next corner. We’ve been through extreme heat, extreme cold, rain, wind and snow.</p>
                            <p>We’ve seen them survive and thrive in these harsh tests, where every component is put under unbearable strain, every join is twisted to its limits and every part of the undercarriage is banged against ruts, banks and rocks repeatedly. </p>
                            <h5>That’s why we’re 100% confident that they’ll handle any adventure you want to take them on. </h5>
                            <p>You want to get to that secluded spot that only the locals know about? Go for it. You want the perfect family or couple trip, with the peace of mind that your camper has been built to last? We’ve got you coverd.  </p>
                        </div>
                    </div>
                </div>
                <div class="white-strip"></div>
            </div>
        </div>
        <div class="bottom-chevron">
            <img src="http://localhost:10003/wp-content/uploads/chevron-bottom.svg" width="100%">
        </div>
        

       <!--<div class="triangle-divider">
            <div class="triangles">
                <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 100 100" preserveAspectRatio="none">
                    <polygon fill="white" points="100,100 0,100, 0,0" />
                </svg>
                <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 100 100" preserveAspectRatio="none">
                    <polygon fill="white" points="0,100 100,0 100,100"></polygon>
                </svg>
            </div>
            <img data-src="/-/media/project/images/g2m/img-sem-hero-min-b-png.png?h=603&amp;w=1044&amp;la=en-AU&amp;hash=05BAE24430B4232FF0E197BB34F7BBA9" alt="" />
        </div>-->
    </div>
    
</div>

<!--<div class="chevron-bottom">

</div>-->