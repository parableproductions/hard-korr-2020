<?php

/**
 * slider-syncing Block Template.
 *
 * @param   array $block The block settings and attributes.
 * @param   string $content The block inner HTML (empty).
 * @param   bool $is_preview True during AJAX preview.
 * @param   (int|string) $post_id The post ID this block is saved to.
 */

// Create id attribute allowing for custom "anchor" value.
$id = 'slider-syncing-' . $block['id'];
if( !empty($block['anchor']) ) {
    $id = $block['anchor'];
}

// Create class attribute allowing for custom "className" and "align" values.
$className = 'slider-syncing';
if( !empty($block['className']) ) {
    $className .= ' ' . $block['className'];
}
if( !empty($block['align']) ) {
    $className .= ' align' . $block['align'];
}

// Load values and assign defaults.
if( have_rows('slick_slider_syncing') ):
?>

<div id="<?php echo esc_attr($id); ?>" class="<?php echo esc_attr($className); ?>">
    <div class="uk-container">
            <div class="uk-grid-medium uk-child-width-expand@l uk-grid uk-grid-stack">
            <div class="slider-syncing-script">
                <?php while ( have_rows( 'slick_slider_syncing' ) ) : the_row(); 
                $slider_img = get_sub_field('slider_syncing_image');
                ?>
                <img class="info_icon" src="<?php echo esc_url($slider_img['url']); ?>"
                    alt="<?php echo esc_attr($slider_img['alt']); ?>">

                <?php endwhile; ?>
            </div>

        </div>
    </div>
    <?php endif; ?>
    
</div>