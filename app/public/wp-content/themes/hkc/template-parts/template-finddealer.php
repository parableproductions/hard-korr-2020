<div id="find-dealer-block_5e532ed31d259" class="find-dealer">


    
    <div class="dealer-container" style="background-image: url('http://hard-korr-campers-2020.local/wp-content/uploads/istock_000008791096large1-scaled.jpg'); background-repeat: no-repeat; background-size: cover; background-attachment: fixed;">
    <div class="overlay-black"></div>
        <div class="uk-container pad-to-bot">
            <div class="wrap">
                <div class="wrap__left">
                <h1>Find A Dealer</h1>
                    
                </div>
                <div class="wrap__right">
                    <div class="right_content">
                        <h5>Our dealer network is rapidly expanding. We now have seven dealerships across Australia, with several more set to launch in the coming months.</h5>
                    <p>We also attend caravan &amp; camping shows around Australia – if you want to check out where we’ll be in the coming months, check out our Upcoming Shows page.</p>
                    </div>
                    <a href="http://hard-korr-campers-2020.local/dealer-location/" target="_blank">
                    <div class="wrapper-map">
                        <div class="copy">
                            <h3>CLICK TO FIND YOUR NEAREST DEALER</h3>
                            <p></p></div>
                            <div class="bg-image">
                            <!-- <img class="alignnone size-medium wp-image-189" src="http://hard-korr-campers-2020.local/wp-content/uploads/australia-300x112.png" alt="" width="300" height="112" srcset="http://hard-korr-campers-2020.local/wp-content/uploads/australia-300x112.png 300w, http://hard-korr-campers-2020.local/wp-content/uploads/australia.png 396w" sizes="(max-width: 300px) 100vw, 300px" />-->
                        </div>
                    </div>
                    </a>
                </div>
                
            </div>
            <div class="white-strip"></div>
        </div>

        <div class="bottom-chevron">
            <img src="http://hard-korr-campers-2020.local/wp-content/uploads/chevron-bottom.svg" width="100%">
           
        </div>
    </div>
</div>