<div class="show-events">
    <div class="show-container">
        <div class="uk-container">
            <div class="wrap">
            <?= do_shortcode('[ecs-list-events cat="Caravan" ]'); ?>
            </div>
        </div>
    </div>
</div>
