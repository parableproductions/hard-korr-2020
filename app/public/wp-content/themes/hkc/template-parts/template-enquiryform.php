<h4 id="contact" class="has-text-align-center">MAKE AN</h4>
<h2 class="has-hkc-black-color has-text-color has-text-align-center">ENQUIRY</h2>
<div class="wp-block-coblocks-row coblocks-row-21111151052" data-columns="1" data-layout="100"><div class="wp-block-coblocks-row__inner has-medium-gutter has-no-padding has-no-margin is-stacked-on-mobile">
<div class="wp-block-coblocks-column coblocks-column-211111511672 uk-block" style="width:100%"><div class="wp-block-coblocks-column__inner has-no-padding has-no-margin">
<div class="wp-block-coblocks-row coblocks-row-21111152692" data-columns="2" data-layout="50-50"><div class="wp-block-coblocks-row__inner has-medium-gutter has-no-padding has-no-margin is-stacked-on-mobile">
<div class="wp-block-coblocks-column coblocks-column-211111528580" style="width:50%"><div class="wp-block-coblocks-column__inner has-no-padding has-no-margin">
<figure class="wp-block-image size-large"><img src="http://hard-korr-campers-2020.local/wp-content/uploads/Hard-Korr-Campers-Logo-RGB.png" alt="" class="wp-image-207" srcset="http://hard-korr-campers-2020.local/wp-content/uploads/Hard-Korr-Campers-Logo-RGB.png 501w, http://hard-korr-campers-2020.local/wp-content/uploads/Hard-Korr-Campers-Logo-RGB-300x60.png 300w" sizes="(max-width: 501px) 100vw, 501px"></figure>



<p class="name-top">Hard Korr Campers Pty Ltd<br><span>ABN 15 611 777 470</span></p>



<p><a href="tel:1300HRDKOR" class="blu-number">1300 HRD KOR</a><br><br>25 Old Pacific Highway<br>YATALA QLD 4207<br>Australia<br><br>PO Box 495<br>ORMEAU QLD 4208<br>Australia<br><br>E:<a href="mailto:sales@hardkorrcampers.com.au"> sales@hardkorrcampers.com.au</a></p>
</div></div>


<?php echo do_shortcode('[contact-form-7 id="211" title="Enquire"]'); ?>

</div></div>
</div></div>
</div></div>


